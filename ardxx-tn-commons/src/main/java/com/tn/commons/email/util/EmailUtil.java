package com.tn.commons.email.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tn.commons.email.dto.Mail;
import com.tn.commons.email.service.EMailService;

/**
 * EmailUtil is a process all kind of email template to the user.
 * 
 * @author naik
 *
 */
@Service
public final class EmailUtil
{
	@Autowired
	private EMailService emailService;

	private static Properties properties = null;
	private static final Logger logger = LoggerFactory.getLogger(EmailUtil.class);

	public EmailUtil()
	{

		if (null == properties)
		{
			properties = new Properties();

			try
			{
				properties.load(EmailUtil.class.getClassLoader().getResourceAsStream("emailutil.properties"));
			} catch (FileNotFoundException e)
			{
				logger.error("FileNotFoundException" + e.getMessage());
			} catch (IOException e)
			{
				logger.error("IOException" + e.getMessage());
			}
		}
	}

	public void createEmailTemplate(final String[] userEmails, final String firstName, final String lastName, final String status)
	{
		try
		{
			Mail mail = new Mail();
			mail.setTemplateName(properties.getProperty("new.protocol.template.name"));
			mail.setMailTo(userEmails);
			mail.setMailFrom(properties.getProperty("from.email.address"));
			mail.setMailSubject(properties.getProperty("new.protocol.email.subject"));

			Map<String, String> mailContent = new HashMap<String, String>();
			mailContent.put("firstName", firstName);
			mailContent.put("lastName", lastName);
			mailContent.put("status", status);
			mail.setMailContent(mailContent);
			emailService.sendEmail(mail);
			logger.info("Successfully email sent for to notify new protocol creation ");
		} catch (Exception e)
		{
			logger.error("Error while sending email. Error message : " + e.getMessage());
		}
	}

	public void createEmailTemplate(final String[] userEmails, final Long protocolId)
	{
		Mail mail = new Mail();

		mail.setTemplateName(properties.getProperty("dmr.review.template.name"));

		mail.setMailTo(userEmails);
		mail.setMailFrom(properties.getProperty("from.email.address"));
		mail.setMailSubject(properties.getProperty("dmr.review.email.subject1") + " " + protocolId + " "
				+ properties.getProperty("dmr.review.email.subject2"));
		Map<String, String> mailContent = new HashMap<String, String>();
		mailContent.put("protocolID", String.valueOf(protocolId));
		mailContent.put("piUser", "");
		mailContent.put("DateTime", new Date().toString());
		mail.setMailContent(mailContent);
		emailService.sendEmail(mail);
		logger.info("Successfully email sent for to notify dmr review ");
	}

	public void createEmailTemplate(final String[] userEmails, final String firstName, final String lastName, final String userName,
			final String password)
	{
		try
		{
			Mail mail = new Mail();
			mail.setTemplateName(properties.getProperty("user.created.templat.ename"));
			mail.setMailTo(userEmails);
			mail.setMailFrom(properties.getProperty("from.email.address"));
			mail.setMailSubject(properties.getProperty("user.created.email.subject"));

			Map<String, String> mailContent = new HashMap<String, String>();
			mailContent.put("firstName", firstName);
			mailContent.put("lastName", lastName);
			mailContent.put("userName", userName);
			mailContent.put("password", password);

			mail.setMailContent(mailContent);
			emailService.sendEmail(mail);
			logger.info("Successfully email sent for to notify user creation ");
		} catch (Exception e)
		{
			logger.error("Error while sending email. Error message : " + e.getMessage());
		}

	}

	public void createEmailTemplate(final String template, final String[] userEmails, final Long protocolId, final String firstName)
	{
		try
		{
			Mail mail = new Mail();
			boolean log = false;
			if (properties.getProperty("review.template.name").equals(template))
			{
				mail.setTemplateName(properties.getProperty("review.template.name"));
				mail.setMailSubject(properties.getProperty("review.email.subject1") + " " + protocolId + " "
						+ properties.getProperty("review.email.subject2"));
				log = true;
			} else
			{
				mail.setTemplateName(properties.getProperty("approved.template.name"));
				mail.setMailSubject(properties.getProperty("approved.email.subject1") + " " + protocolId + " "
						+ properties.getProperty("approved.email.subject2"));

			}
			mail.setMailFrom(properties.getProperty("from.email.address"));
			mail.setMailTo(userEmails);
			Map<String, String> mailContent = new HashMap<String, String>();
			mailContent.put("protocolID", String.valueOf(protocolId));
			mailContent.put("piUser", firstName);
			mailContent.put("DateTime", new Date().toString());

			mail.setMailContent(mailContent);
			emailService.sendEmail(mail);
			if (log)
			{
				logger.info("Successfully email sent for to notify review protocol");
			} else
			{
				logger.info("Successfully email sent for to notify approve protocol ");
			}
		} catch (Exception e)
		{
			logger.error("Error while sending email. Error message : " + e.getMessage());
		}

	}

}
