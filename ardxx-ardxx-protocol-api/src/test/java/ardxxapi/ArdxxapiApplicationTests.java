package ardxxapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ardxx.ardxxapi.ArdxxapiApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ArdxxapiApplication.class)
@WebAppConfiguration
public class ArdxxapiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
