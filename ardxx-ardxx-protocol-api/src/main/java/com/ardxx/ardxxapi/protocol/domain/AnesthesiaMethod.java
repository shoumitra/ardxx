package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class AnesthesiaMethod implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER, orphanRemoval = true)
	private UserInput anesthesiaMethodDesc;

	private boolean none;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public boolean isNone()
	{
		return none;
	}

	public void setNone(boolean none)
	{
		this.none = none;
	}

	public UserInput getAnesthesiaMethodDesc()
	{
		return anesthesiaMethodDesc;
	}

	public void setAnesthesiaMethodDesc(UserInput anesthesiaMethodDesc)
	{
		this.anesthesiaMethodDesc = anesthesiaMethodDesc;
	}

}
