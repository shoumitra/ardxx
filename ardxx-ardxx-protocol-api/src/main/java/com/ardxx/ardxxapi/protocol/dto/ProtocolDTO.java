package com.ardxx.ardxxapi.protocol.dto;

import java.io.Serializable;
import java.util.Date;

import com.tn.security.basic.domain.User;

public class ProtocolDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;

	private String title;
	private String proposalNumber;
	private Date approvalDate;
	private Date expirationDate;
	private Date annualReviewDueFirstYear;
	private Date annualReviewDueSecondYear;
	private String status;
	private Date createdDate;
	private User piUser;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getProposalNumber()
	{
		return proposalNumber;
	}

	public void setProposalNumber(String proposalNumber)
	{
		this.proposalNumber = proposalNumber;
	}

	public Date getApprovalDate()
	{
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate)
	{
		this.approvalDate = approvalDate;
	}

	public Date getExpirationDate()
	{
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate)
	{
		this.expirationDate = expirationDate;
	}

	public Date getAnnualReviewDueFirstYear()
	{
		return annualReviewDueFirstYear;
	}

	public void setAnnualReviewDueFirstYear(Date annualReviewDueFirstYear)
	{
		this.annualReviewDueFirstYear = annualReviewDueFirstYear;
	}

	public Date getAnnualReviewDueSecondYear()
	{
		return annualReviewDueSecondYear;
	}

	public void setAnnualReviewDueSecondYear(Date annualReviewDueSecondYear)
	{
		this.annualReviewDueSecondYear = annualReviewDueSecondYear;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public User getPiUser()
	{
		return piUser;
	}

	public void setPiUser(User piUser)
	{
		this.piUser = piUser;
	}

}
