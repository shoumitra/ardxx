package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PICertification implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String principaInvestigatorName;
	private String signature;
	private Date date;

	@ElementCollection
	private List<UserInput> userInputs;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getPrincipaInvestigatorName()
	{
		return principaInvestigatorName;
	}

	public void setPrincipaInvestigatorName(String principaInvestigatorName)
	{
		this.principaInvestigatorName = principaInvestigatorName;
	}

	public String getSignature()
	{
		return signature;
	}

	public void setSignature(String signature)
	{
		this.signature = signature;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public List<UserInput> getUserInputs()
	{
		return userInputs;
	}

	public void setUserInputs(List<UserInput> userInputs)
	{
		this.userInputs = userInputs;
	}

}
