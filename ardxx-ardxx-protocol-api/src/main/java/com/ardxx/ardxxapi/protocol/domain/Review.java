package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.tn.security.basic.domain.User;

@Entity
public class Review implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private boolean isDesignatedMemberReview;

	@ElementCollection
	private List<User> userNames;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public boolean isDesignatedMemberReview()
	{
		return isDesignatedMemberReview;
	}

	public void setDesignatedMemberReview(boolean isDesignatedMemberReview)
	{
		this.isDesignatedMemberReview = isDesignatedMemberReview;
	}

	public List<User> getUserNames()
	{
		return userNames;
	}

	public void setUserNames(List<User> userNames)
	{
		this.userNames = userNames;
	}

}
