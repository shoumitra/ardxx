/**
 * 
 */
package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;

import javax.persistence.*;

/**
 * @author gopikrishnappa
 *
 */
@Entity
public class Location implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String country;

	private String phone;
	private String email;

	@Enumerated(EnumType.STRING)
	@Column(name = "location_type")
	private LocationType locationType;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStreet()
	{
		return street;
	}

	public void setStreet(String street)
	{
		this.street = street;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getZip()
	{
		return zip;
	}

	public void setZip(String zip)
	{
		this.zip = zip;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public LocationType getLocationType()
	{
		return locationType;
	}

	public void setLocationType(LocationType locationType)
	{
		this.locationType = locationType;
	}

}
