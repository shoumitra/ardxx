package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Entity to hold biological material/animal products for use in animals .
 * 
 * @author gopikrishnappa
 *
 */
@Entity
public class BioAnimalMaterial implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private boolean none;

	@ElementCollection
	private List<BioMaterialData> bioMaterialsUsed;
	private String initialsOfPI;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public boolean isNone()
	{
		return none;
	}

	public void setNone(boolean none)
	{
		this.none = none;
	}

	public List<BioMaterialData> getBioMaterialsUsed()
	{
		return bioMaterialsUsed;
	}

	public void setBioMaterialsUsed(List<BioMaterialData> bioMaterialsUsed)
	{
		this.bioMaterialsUsed = bioMaterialsUsed;
	}

	public String getInitialsOfPI()
	{
		return initialsOfPI;
	}

	public void setInitialsOfPI(String initialsOfPI)
	{
		this.initialsOfPI = initialsOfPI;
	}

}
