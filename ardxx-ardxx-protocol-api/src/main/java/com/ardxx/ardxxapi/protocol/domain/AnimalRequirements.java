/**
 * 
 */
package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * @author gopikrishnappa
 *
 */
@Entity
public class AnimalRequirements implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String species;
	private String strain;
	private String sources;
	private String age;
	private String sex;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private NumberOfAnimals numberOfAnimals;
	private String holdingLocations;
	private String procedureLocations;

	private String bacteriologyOrViral;

	private String specialWater;
	private String specialDiet;
	private String specialBedding;
	private String environmentalRequirementsF;
	private String environmentalRequirementsRh;
	private String lifeCycleRequirementsHoursOn;
	private String lifeCycleRequirementsHoursOff;
	private String specialLightning;
	private boolean animalsIrradiated = false;
	private String specialCagingEquipment;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getSpecies()
	{
		return species;
	}

	public void setSpecies(String species)
	{
		this.species = species;
	}

	public String getStrain()
	{
		return strain;
	}

	public void setStrain(String strain)
	{
		this.strain = strain;
	}

	public String getSources()
	{
		return sources;
	}

	public void setSources(String sources)
	{
		this.sources = sources;
	}

	public String getAge()
	{
		return age;
	}

	public void setAge(String age)
	{
		this.age = age;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public NumberOfAnimals getNumberOfAnimals()
	{
		return numberOfAnimals;
	}

	public void setNumberOfAnimals(NumberOfAnimals numberOfAnimals)
	{
		this.numberOfAnimals = numberOfAnimals;
	}

	public String getHoldingLocations()
	{
		return holdingLocations;
	}

	public void setHoldingLocations(String holdingLocations)
	{
		this.holdingLocations = holdingLocations;
	}

	public String getProcedureLocations()
	{
		return procedureLocations;
	}

	public void setProcedureLocations(String procedureLocations)
	{
		this.procedureLocations = procedureLocations;
	}

	public String getBacteriologyOrViral()
	{
		return bacteriologyOrViral;
	}

	public void setBacteriologyOrViral(String bacteriologyOrViral)
	{
		this.bacteriologyOrViral = bacteriologyOrViral;
	}

	public String getSpecialWater()
	{
		return specialWater;
	}

	public void setSpecialWater(String specialWater)
	{
		this.specialWater = specialWater;
	}

	public String getSpecialDiet()
	{
		return specialDiet;
	}

	public void setSpecialDiet(String specialDiet)
	{
		this.specialDiet = specialDiet;
	}

	public String getSpecialBedding()
	{
		return specialBedding;
	}

	public void setSpecialBedding(String specialBedding)
	{
		this.specialBedding = specialBedding;
	}

	public String getEnvironmentalRequirementsF()
	{
		return environmentalRequirementsF;
	}

	public void setEnvironmentalRequirementsF(String environmentalRequirementsF)
	{
		this.environmentalRequirementsF = environmentalRequirementsF;
	}

	public String getEnvironmentalRequirementsRh()
	{
		return environmentalRequirementsRh;
	}

	public void setEnvironmentalRequirementsRh(String environmentalRequirementsRh)
	{
		this.environmentalRequirementsRh = environmentalRequirementsRh;
	}

	public String getLifeCycleRequirementsHoursOn()
	{
		return lifeCycleRequirementsHoursOn;
	}

	public void setLifeCycleRequirementsHoursOn(String lifeCycleRequirementsHoursOn)
	{
		this.lifeCycleRequirementsHoursOn = lifeCycleRequirementsHoursOn;
	}

	public String getLifeCycleRequirementsHoursOff()
	{
		return lifeCycleRequirementsHoursOff;
	}

	public void setLifeCycleRequirementsHoursOff(String lifeCycleRequirementsHoursOff)
	{
		this.lifeCycleRequirementsHoursOff = lifeCycleRequirementsHoursOff;
	}

	public String getSpecialLightning()
	{
		return specialLightning;
	}

	public void setSpecialLightning(String specialLightning)
	{
		this.specialLightning = specialLightning;
	}

	public boolean isAnimalsIrradiated()
	{
		return animalsIrradiated;
	}

	public void setAnimalsIrradiated(boolean animalsIrradiated)
	{
		this.animalsIrradiated = animalsIrradiated;
	}

	public String getSpecialCagingEquipment()
	{
		return specialCagingEquipment;
	}

	public void setSpecialCagingEquipment(String specialCagingEquipment)
	{
		this.specialCagingEquipment = specialCagingEquipment;
	}

	/*
	 * public Protocol getProtocol() { return protocol; }
	 * 
	 * public void setProtocol(Protocol protocol) { this.protocol = protocol; }
	 */

}
