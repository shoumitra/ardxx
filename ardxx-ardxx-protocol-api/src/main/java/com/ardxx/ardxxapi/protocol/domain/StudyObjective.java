package com.ardxx.ardxxapi.protocol.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StudyObjective implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ElementCollection
	private List<UserInput> userInputs;

	private boolean none;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public List<UserInput> getUserInputs()
	{
		return userInputs;
	}

	public void setUserInputs(List<UserInput> userInputs)
	{
		this.userInputs = userInputs;
	}

	public boolean isNone()
	{
		return none;
	}

	public void setNone(boolean none)
	{
		this.none = none;
	}

}
