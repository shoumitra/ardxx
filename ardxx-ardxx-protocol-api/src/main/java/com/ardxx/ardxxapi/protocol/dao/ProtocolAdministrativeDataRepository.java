package com.ardxx.ardxxapi.protocol.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.protocol.domain.ProtocolAdministrativeData;

public interface ProtocolAdministrativeDataRepository extends PagingAndSortingRepository<ProtocolAdministrativeData, Long>
{

}
