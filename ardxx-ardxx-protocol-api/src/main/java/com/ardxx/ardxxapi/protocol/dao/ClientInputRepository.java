package com.ardxx.ardxxapi.protocol.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.protocol.domain.ClientInput;

public interface ClientInputRepository extends PagingAndSortingRepository<ClientInput, Long>
{

}
