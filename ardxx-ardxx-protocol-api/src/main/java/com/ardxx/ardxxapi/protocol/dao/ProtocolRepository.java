package com.ardxx.ardxxapi.protocol.dao;

import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.ardxx.ardxxapi.protocol.domain.Protocol;
import com.ardxx.ardxxapi.protocol.domain.ProtocolStatus;

@RepositoryRestResource(collectionResourceRel = "protocol", path = "protocol")
public interface ProtocolRepository extends PagingAndSortingRepository<Protocol, Long>, ProtocolRepositoryCustom
{

	Protocol findByProposalNumber(@Param("proposalNumber") String proposalNumber);

	Protocol findByStudyObjectiveId(@Param("id") Long id);

	Protocol findByRationalUseOfAnimalsId(@Param("id") Long id);

	Protocol findByMajorSurvialSurgeryId(@Param("id") Long id);

	Protocol findByDescriptionOfExperimentId(@Param("id") Long id);

	Protocol findByProtocolAdministrativeDataId(@Param("id") Long id);

	Protocol findByRecordPainDistressCategoryId(@Param("id") Long id);

	Protocol findBySpecialRequirementOfStudyId(@Param("id") Long id);

	Protocol findByTransportationId(@Param("id") Long id);

	Protocol findByEuthanasiaMethodId(@Param("id") Long id);

	Protocol findByAnesthesiaMethodId(@Param("id") Long id);

	Protocol findByHazardousAgentsId(@Param("id") Long id);

	Protocol findByBioAnimalMaterialId(@Param("id") Long id);

	Protocol findByPICertificationId(@Param("id") Long id);

	Iterable<Protocol> findByCreatedById(@Param("id") Long id);
	
	Iterable<Protocol> findByPiUserId(@Param("id") Long id);

	List<Protocol> findByPiUserIdAndStatus(@Param("pi") Long pi, @Param("status") ProtocolStatus status);
}