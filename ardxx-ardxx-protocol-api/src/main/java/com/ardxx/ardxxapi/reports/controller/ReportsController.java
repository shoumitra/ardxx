package com.ardxx.ardxxapi.reports.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ardxx.ardxxapi.exception.ARDXXAppException;
import com.ardxx.ardxxapi.exception.ARDXXErrorPayload;
import com.ardxx.ardxxapi.reports.dto.QueryBuilder;
import com.ardxx.ardxxapi.reports.service.ReportsService;

/**
 * 
 * @author Raj
 *
 */
@RestController
@RequestMapping(value = "/reports")
public class ReportsController
{
	@Autowired
	ReportsService reportsService;

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Object> getReport(@RequestBody QueryBuilder queryBuilder)
	{
		try
		{
			List<Map<String, Object>> response = reportsService.getReports(queryBuilder);
			// Study study = studyService.createStudy(studyDTO);
			return new ResponseEntity<Object>(response, HttpStatus.OK);
		} catch (Exception exception)
		{
			exception.printStackTrace();
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}

	@RequestMapping(value = "/canned", method = RequestMethod.GET)
	public ResponseEntity<Object> getCannedReports(@RequestParam("reportid") Long id, @RequestParam("searchid") String searchId,
			@RequestParam("fromdate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
			@RequestParam("todate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate)
	{
		try
		{
			List<?> response = reportsService.getCannedReports(id, searchId, fromDate, toDate);
			return new ResponseEntity<Object>(response, HttpStatus.OK);
		} catch (Exception exception)
		{
			exception.printStackTrace();
			if (exception instanceof ARDXXAppException)
			{
				return new ResponseEntity<Object>(new ARDXXErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

	}
}
