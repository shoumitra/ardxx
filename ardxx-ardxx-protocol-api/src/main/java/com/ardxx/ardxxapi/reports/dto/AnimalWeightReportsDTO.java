package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;

public class AnimalWeightReportsDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long animalId;
	private String weights;

	public Long getAnimalId()
	{
		return animalId;
	}

	public void setAnimalId(Long animalId)
	{
		this.animalId = animalId;
	}

	public String getWeights()
	{
		return weights;
	}

	public void setWeights(String weights)
	{
		this.weights = weights;
	}

}
