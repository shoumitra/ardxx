package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;

/**
 * @author naik
 *
 */
public class AnimalHistoryCannedReportDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String animalId;
	private String fileId;
	private String sex;

	public String getAnimalId()
	{
		return animalId;
	}

	public void setAnimalId(String animalId)
	{
		this.animalId = animalId;
	}

	public String getFileId()
	{
		return fileId;
	}

	public void setFileId(String fileId)
	{
		this.fileId = fileId;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	

}
