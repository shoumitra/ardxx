package com.ardxx.ardxxapi.reports.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.BloodSample;
import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureSchedule;
import com.ardxx.ardxxapi.animalmanagement.domain.Study;
import com.ardxx.ardxxapi.animalmanagement.domain.StudyProcedure;
import com.ardxx.ardxxapi.common.ReportUtil;
import com.ardxx.ardxxapi.protocol.domain.Protocol;
import com.ardxx.ardxxapi.reports.dto.AnimalHistoryCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.AnimalJobCodeDetailReportsDTO;
import com.ardxx.ardxxapi.reports.dto.AnimalWeightReportsDTO;
import com.ardxx.ardxxapi.reports.dto.CBCCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.QueryBuilder;
import com.ardxx.ardxxapi.reports.dto.ReportDTO;
import com.ardxx.ardxxapi.reports.dto.RoomHistoryCannedReportDTO;
import com.ardxx.ardxxapi.reports.dto.WhereClause;

@Repository
public class ReportsDAOImpl implements ReportsDAO, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Map<String, Object>> getReports(QueryBuilder queryBuilder)
	{
		List<ReportDTO> reportDTOs = queryBuilder.getEntities();
		List<Map<String, Object>> response = new ArrayList<Map<String, Object>>();

		List<String> responseColumns = new ArrayList<String>();

		CriteriaBuilder qb = entityManager.getCriteriaBuilder();

		ReportDTO study = new ReportDTO();
		study.setTable(ReportUtil.STUDY);
		boolean isStudy = false;

		ReportDTO procedure = new ReportDTO();
		procedure.setTable(ReportUtil.PROCEDURE);
		boolean isProcedure = false;

		ReportDTO schedule = new ReportDTO();
		schedule.setTable(ReportUtil.SCHEDULE);
		boolean isSchedule = false;

		ReportDTO animal = new ReportDTO();
		animal.setTable(ReportUtil.ANIMALS);
		boolean isAnimal = false;

		ReportDTO protocol = new ReportDTO();
		protocol.setTable(ReportUtil.PROTOCOL);
		boolean isProtocol = false;

		CriteriaQuery cq = null;
		Root root = null;
		ReportDTO reportDTO = null;
		List<Path> columnList = new ArrayList<Path>();
		List<Predicate> whereCondition = new ArrayList<Predicate>();

		int columnIndex = 0;

		if (reportDTOs.contains(study))
		{
			cq = qb.createQuery(Study.class);
			root = cq.from(Study.class);

			whereCondition.add(qb.equal(root.get("isDraft"), false));
			whereCondition.add(qb.equal(root.get("isClinical"), false));

			for (ReportDTO dTO : reportDTOs)
			{
				if (dTO.equals(study))
				{
					reportDTO = dTO;
					for (String columns : reportDTO.getColumns())
					{
						columnList.add(root.get(columns));
						responseColumns.add(columnIndex, "study." + columns);
						columnIndex++;

					}
					break;
				}
			}

			if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
			{

				for (WhereClause where : reportDTO.getWhereClause())
				{
					whereForRootClass(qb, root, whereCondition, where);
				}
			}

			isStudy = true;
		}

		Join<Study, Protocol> protocolJoin = null;

		if (reportDTOs.contains(protocol))
		{
			if (isStudy)
			{
				protocolJoin = root.join("protocol");
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(protocol))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(protocolJoin.get(columns));
							responseColumns.add(columnIndex, "protocol." + columns);
							columnIndex++;
						}
						break;
					}

				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForJoin(protocolJoin, whereCondition, qb, where, where.getColumn());
					}
				}
			} else
			{
				cq = qb.createQuery(Protocol.class);
				root = cq.from(Protocol.class);
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(protocol))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(root.get(columns));
							responseColumns.add(columnIndex, "protocol." + columns);
							columnIndex++;

						}
						break;
					}
				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForRootClass(qb, root, whereCondition, where);
					}
				}

			}
		}

		Join<StudyProcedure, Study> procedureJoin = null;

		if (reportDTOs.contains(procedure))
		{
			List<String> procedureAnimals = new ArrayList<String>();
			List<WhereClause> procedureAnimalsWhere = new ArrayList<WhereClause>();
			List<String> bloodSamples = new ArrayList<String>();
			List<WhereClause> bloodSamplesWhere = new ArrayList<WhereClause>();
			if (isStudy)
			{
				procedureJoin = root.join("procedures");

				columnIndex = setStudyProcedureJoins(reportDTOs, procedure, reportDTO, procedureAnimals, bloodSamples, columnList, procedureJoin,
						null, responseColumns, columnIndex, procedureAnimalsWhere, bloodSamplesWhere, whereCondition, qb);

			} else
			{
				cq = qb.createQuery(StudyProcedure.class);
				root = cq.from(StudyProcedure.class);

				columnIndex = setStudyProcedureJoins(reportDTOs, procedure, reportDTO, procedureAnimals, bloodSamples, columnList, null, root,
						responseColumns, columnIndex, procedureAnimalsWhere, bloodSamplesWhere, whereCondition, qb);

			}

			isProcedure = true;

		}

		if (reportDTOs.contains(schedule))
		{
			for (ReportDTO dTO : reportDTOs)
			{
				if (dTO.equals(schedule))
				{
					reportDTO = dTO;
					break;
				}
			}

			Join<ProcedureSchedule, Study> scheduleJoin = null;
			if (isStudy && !isProcedure)
			{
				scheduleJoin = root.join("procedures").join("procedureSchedules");
				for (String columns : reportDTO.getColumns())
				{
					columnList.add(scheduleJoin.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}
			if (!isStudy && isProcedure)
			{
				scheduleJoin = root.join("procedureSchedules");
				for (String columns : reportDTO.getColumns())
				{
					columnList.add(scheduleJoin.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}
			if (isStudy && isProcedure)
			{
				scheduleJoin = procedureJoin.join("procedureSchedules");
				for (String columns : reportDTO.getColumns())
				{
					columnList.add(scheduleJoin.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}
			if (!isStudy && !isProcedure)
			{
				cq = qb.createQuery(ProcedureSchedule.class);
				root = cq.from(ProcedureSchedule.class);

				for (String columns : reportDTO.getColumns())
				{
					columnList.add(root.get(columns));
					responseColumns.add(columnIndex, "schedule." + columns);
					columnIndex++;
				}
			}

			if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
			{

				for (WhereClause where : reportDTO.getWhereClause())
				{
					if (scheduleJoin != null)
					{
						whereForJoin(scheduleJoin, whereCondition, qb, where, where.getColumn());
					} else
					{
						whereForRootClass(qb, root, whereCondition, where);
					}

				}
			}
			isSchedule = true;
		}

		if (reportDTOs.contains(animal))
		{
			if (isProcedure)
			{

				Join<StudyProcedure, Animals> procedureAnimalJoin = root.join("animals");
				;
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(animal))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(procedureAnimalJoin.get(columns));
							responseColumns.add(columnIndex, "animals." + columns);
							columnIndex++;
						}
						break;
					}

				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForJoin(procedureAnimalJoin, whereCondition, qb, where, where.getColumn());
					}
				}
			} else if (isStudy)
			{
				Join<Study, Animals> studyAnimalsJoin = null;
				if (isProcedure)
				{
					studyAnimalsJoin = procedureJoin.join("animals");
				} else
				{
					studyAnimalsJoin = root.join("animals");
				}

				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(animal))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(studyAnimalsJoin.get(columns));
							responseColumns.add(columnIndex, "animals." + columns);
							columnIndex++;
						}
						break;
					}

				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForJoin(studyAnimalsJoin, whereCondition, qb, where, where.getColumn());
					}
				}
			}

			else
			{
				cq = qb.createQuery(Animals.class);
				root = cq.from(Animals.class);
				for (ReportDTO dTO : reportDTOs)
				{
					if (dTO.equals(animal))
					{
						reportDTO = dTO;
						for (String columns : reportDTO.getColumns())
						{
							columnList.add(root.get(columns));
							responseColumns.add(columnIndex, "animals." + columns);
							columnIndex++;
						}
						break;
					}
				}

				if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
				{

					for (WhereClause where : reportDTO.getWhereClause())
					{
						whereForRootClass(qb, root, whereCondition, where);
					}
				}
			}

			isAnimal = true;

		}

		Path[] paths = new Path[columnList.size()];

		for (int i = 0; i < columnList.size(); i++)
		{
			paths[i] = columnList.get(i);
		}

		Predicate[] predicates = new Predicate[whereCondition.size()];

		for (int i = 0; i < whereCondition.size(); i++)
		{
			predicates[i] = whereCondition.get(i);
		}

		cq.select(qb.array(paths));
		cq.where(predicates);

		TypedQuery<Object> typedQuery = entityManager.createQuery(cq);
		List<Object> resultlist = typedQuery.getResultList();
		for (Object object : resultlist)
		{
			LinkedHashMap<String, Object> row = new LinkedHashMap<String, Object>();
			if (object != null)
			{
				if (object instanceof Object[])
				{
					Object[] resultArray = (Object[]) object;

					for (String string : queryBuilder.getColumnOrder())
					{
						for (int i = 0; i < resultArray.length; i++)
						{

							if (string.equalsIgnoreCase(responseColumns.get(i)))
							{
								Object obj = resultArray[i];

								if (obj instanceof Date)
								{
									Date date = (Date) obj;
									row.put(responseColumns.get(i), new SimpleDateFormat("MM/dd/yyyy").format(date));
								} else
								{
									row.put(responseColumns.get(i), resultArray[i]);
								}

								break;
							}

						}
					}
				} else if (object instanceof String)
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				} else if (object instanceof Long)
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				} else
				{
					row.put(queryBuilder.getColumnOrder().get(0), object.toString());
				}
			}

			response.add(row);
		}

		return response;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private int setStudyProcedureJoins(List<ReportDTO> reportDTOs, ReportDTO procedure, ReportDTO reportDTO, List<String> procedureAnimals,
			List<String> bloodSamples, List<Path> columnList, Join join, Root root, List<String> responseColumns, int columnIndex,
			List<WhereClause> procedureAnimalsWhere, List<WhereClause> bloodSamplesWhere, List<Predicate> whereCondition, CriteriaBuilder qb)
	{
		for (ReportDTO dTO : reportDTOs)
		{
			if (dTO.equals(procedure))
			{
				reportDTO = dTO;
				for (String columns : reportDTO.getColumns())
				{
					if (columns.startsWith("bloodSample"))
					{
						bloodSamples.add(columns);
					} else
					{
						if (root != null)
						{
							columnList.add(root.get(columns));
						} else
						{
							columnList.add(join.get(columns));
						}

						responseColumns.add(columnIndex, "procedure." + columns);
						columnIndex++;
					}

				}
				break;
			}
		}

		if (reportDTO.getWhereClause() != null && !reportDTO.getWhereClause().isEmpty())
		{
			for (WhereClause where : reportDTO.getWhereClause())
			{
				if (where.getColumn().startsWith("bloodSample"))
				{
					bloodSamplesWhere.add(where);
				} else
				{
					if (root != null)
					{
						whereForRootClass(qb, root, whereCondition, where);
					} else
					{
						whereForJoin(join, whereCondition, qb, where, where.getColumn());
					}

				}

			}
		}

		Join<StudyProcedure, BloodSample> bloodSamplesJoin = null;
		if (!bloodSamples.isEmpty())
		{
			if (root != null)
			{
				bloodSamplesJoin = root.join("bloodSample");
			} else
			{
				bloodSamplesJoin = join.join("bloodSample");
			}

			for (String string : bloodSamples)
			{
				string = string.replace("bloodSample.", "");
				columnList.add(bloodSamplesJoin.get(string));
				responseColumns.add(columnIndex, "procedure.bloodSample." + string);
				columnIndex++;
			}
		}

		if (!bloodSamples.isEmpty())
		{
			if (bloodSamplesJoin == null)
			{
				if (root != null)
				{
					bloodSamplesJoin = root.join("bloodSample");
				} else
				{
					bloodSamplesJoin = join.join("bloodSample");
				}

			}

			for (WhereClause where : bloodSamplesWhere)
			{
				String column = where.getColumn().replace("bloodSample.", "");
				whereForJoin(bloodSamplesJoin, whereCondition, qb, where, column);
			}
		}

		return columnIndex;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void whereForJoin(Join join, List<Predicate> whereCondition, CriteriaBuilder qb, WhereClause where, String column)
	{
		if (where.getType().equalsIgnoreCase("EQUAL"))
		{
			whereCondition.add(qb.equal(join.get(column), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("ENDSWITH"))
		{
			whereCondition.add(qb.like(join.get(column), "%" + where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("STARTSWITH"))
		{
			whereCondition.add(qb.like(join.get(column), where.getValue() + "%"));
		}
		if (where.getType().equalsIgnoreCase("BETWEEN"))
		{
			boolean isNumber;
			boolean isDate;

			Long value1 = null;
			Long value2 = null;

			Date fromDate = null;
			Date toDate = null;

			try
			{
				value1 = Long.parseLong(where.getValue());
				value2 = Long.parseLong(where.getOtherValue());
				isNumber = true;
			} catch (NumberFormatException e)
			{
				isNumber = false;
			}

			try
			{
				DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				fromDate = format.parse(where.getValue());
				toDate = format.parse(where.getOtherValue());
				isDate = true;
			} catch (Exception e)
			{
				isDate = false;
			}

			if (isNumber)
			{
				whereCondition.add(qb.between(join.get(column), value1, value2));
			} else if (isDate)
			{
				whereCondition.add(qb.between(join.get(column), fromDate, toDate));
			}

		}
		if (where.getType().equalsIgnoreCase("GREATERTHAN"))
		{
			whereCondition.add(qb.greaterThan(join.get(column), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("LESSTHAN"))
		{
			whereCondition.add(qb.lessThan(join.get(column), where.getValue()));
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void whereForRootClass(CriteriaBuilder qb, Root root, List<Predicate> whereCondition, WhereClause where)
	{
		if (where.getType().equalsIgnoreCase("EQUAL"))
		{
			whereCondition.add(qb.equal(root.get(where.getColumn()), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("STARTSWITH"))
		{
			whereCondition.add(qb.like(root.get(where.getColumn()), where.getValue() + "%"));
		}
		if (where.getType().equalsIgnoreCase("ENDSWITH"))
		{
			whereCondition.add(qb.like(root.get(where.getColumn()), "%" + where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("BETWEEN"))
		{

			boolean isNumber;
			boolean isDate;

			Long value1 = null;
			Long value2 = null;

			Date fromDate = null;
			Date toDate = null;

			try
			{
				value1 = Long.parseLong(where.getValue());
				value2 = Long.parseLong(where.getOtherValue());
				isNumber = true;
			} catch (NumberFormatException e)
			{
				isNumber = false;
			}

			try
			{
				DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				fromDate = format.parse(where.getValue());
				toDate = format.parse(where.getOtherValue());
				isDate = true;
			} catch (Exception e)
			{
				isDate = false;
			}

			if (isNumber)
			{
				whereCondition.add(qb.between(root.get(where.getColumn()), value1, value2));
			} else if (isDate)
			{
				whereCondition.add(qb.between(root.get(where.getColumn()), fromDate, toDate));
			}
		}
		if (where.getType().equalsIgnoreCase("GREATERTHAN"))
		{
			whereCondition.add(qb.greaterThan(root.get(where.getColumn()), where.getValue()));
		}
		if (where.getType().equalsIgnoreCase("LESSTHAN"))
		{
			whereCondition.add(qb.lessThan(root.get(where.getColumn()), where.getValue()));
		}
	}

	@Override
	public List<?> getRoomHistoryDetailsOfAnimal(final String animalId)
	{

		Query query = entityManager.createQuery("SELECT roomNum,recievedDate,departedDate FROM AnimalHistory WHERE animalId=?");
		query.setParameter(1, animalId);
		List<Object[]> historyList = query.getResultList();
		List<RoomHistoryCannedReportDTO> roomHistoryCannedReports = new ArrayList<RoomHistoryCannedReportDTO>();

		for (Object[] list : historyList)
		{
			RoomHistoryCannedReportDTO roomHistoryCannedReportDTO = new RoomHistoryCannedReportDTO();
			if (list != null && list.length > 0)
			{
				roomHistoryCannedReportDTO.setRoom(list[0].toString());
				if (list.length > 1)
				{
					if (list[1] instanceof Date)
					{
						roomHistoryCannedReportDTO.setDateEntered((Date) list[1]);
					}

				}
				if (list.length > 2)
				{
					if (list[2] instanceof Date)
					{
						roomHistoryCannedReportDTO.setDateExit((Date) list[2]);
					}

				}
			}

			roomHistoryCannedReports.add(roomHistoryCannedReportDTO);
		}

		return roomHistoryCannedReports;
	}

	private String getFormatedDate(Date date)
	{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		try
		{
			return simpleDateFormat.format(date);
		} catch (Exception e)
		{
			return "";
		}

	}

	@Override
	public List<?> getAnimalDetailsInRoom(final String roomId)
	{

		Query query = entityManager.createQuery("SELECT animalId, sex,studyId FROM Animals a WHERE roomNum=?");
		query.setParameter(1, roomId);
		List<Object[]> historyList = query.getResultList();
		List<AnimalHistoryCannedReportDTO> animalHistoryCannedReports = new ArrayList<AnimalHistoryCannedReportDTO>();

		for (Object[] list : historyList)
		{
			AnimalHistoryCannedReportDTO animalHistoryCannedReportDTO = new AnimalHistoryCannedReportDTO();
			if (list != null && list.length > 0)
			{
				animalHistoryCannedReportDTO.setAnimalId(list[0].toString());
				if (list.length > 1)
				{
					animalHistoryCannedReportDTO.setSex(list[1].toString());
				}
				if (list.length > 2)
				{
					if (list[2] != null)
					{
						animalHistoryCannedReportDTO.setFileId(list[2].toString());
					} else
					{
						animalHistoryCannedReportDTO.setFileId("");
					}
				}

			}

			animalHistoryCannedReports.add(animalHistoryCannedReportDTO);
		}

		return animalHistoryCannedReports;
	}

	@Override
	public List<?> getCbcCannedReport(final String studyId)
	{

		Query query = entityManager
				.createQuery("SELECT  at.animalId.id,at.collectedDate,at.wbc,at.rbc,at.hgb,at.hct,at.mcv,at.mch,at.mchc,at.nrbc,at.neutrophilSeg,"
						+ "at.neutrophilBand,at.lymphocyte,at.monocyte,at.eosinophil,at.basophil,at.metamyelocyte,"
						+ "at.myelocyte,at.promyelocyte,at.plateletEstimate,at.polychromasia,at.anisocytosis,at.poikilocytosis,at.heinzBodies,"
						+ "at.remarks,at.absoluteNeutrophilSeg,at.absoluteNeutrophilBand,at.absoluteLymphocyte,"
						+ "at.absoluteMonocyte,at.absoluteEosinophil,at.absoluteBasophil,at.absoluteMetamyelocytes,"
						+ "at.absolutePromyelocytes,at.plateletCount FROM AnimalTestResults at,Study s JOIN s.animals sa "
						+ "WHERE sa.id=at.animalId.id AND s.id=?");
		query.setParameter(1, Long.parseLong(studyId));
		List<Object[]> historyList = query.getResultList();
		List<CBCCannedReportDTO> cbcCannedReports = new ArrayList<CBCCannedReportDTO>();

		for (Object[] list : historyList)
		{
			CBCCannedReportDTO cbcCannedReportDTO = new CBCCannedReportDTO();
			if (list != null && list.length > 0)
			{
				if (list[0] instanceof Long)
				{
					cbcCannedReportDTO.setAnimalId((Long) list[0]);
				} else if (list[0] instanceof String)
				{
					cbcCannedReportDTO.setAnimalId(Long.parseLong(list[0].toString()));
				}

				if (list.length > 1)
				{
					if (list[1] instanceof Date)
					{
						cbcCannedReportDTO.setCollectedDate((Date) list[1]);
					}

				}
				if (list.length > 2)
				{
					cbcCannedReportDTO.setWbc(list[2].toString());
				}
				if (list.length > 3)
				{
					cbcCannedReportDTO.setRbc(list[3].toString());
				}
				if (list.length > 4)
				{
					cbcCannedReportDTO.setHgb(list[4].toString());
				}
				if (list.length > 5)
				{
					cbcCannedReportDTO.setHct(list[5].toString());
				}
				if (list.length > 6)
				{
					cbcCannedReportDTO.setMcv(list[6].toString());
				}
				if (list.length > 7)
				{
					cbcCannedReportDTO.setMch(list[7].toString());
				}
				if (list.length > 8)
				{
					cbcCannedReportDTO.setMchc(list[8].toString());
				}
				if (list.length > 9)
				{
					cbcCannedReportDTO.setNrbc(list[9].toString());
				}
				if (list.length > 10)
				{
					cbcCannedReportDTO.setNeutrophilSeg(list[10].toString());
				}
				if (list.length > 11)
				{
					cbcCannedReportDTO.setNeutrophilBand(list[11].toString());
				}
				if (list.length > 12)
				{
					cbcCannedReportDTO.setLymphocyte(list[12].toString());
				}
				if (list.length > 13)
				{
					cbcCannedReportDTO.setMonocyte(list[13].toString());
				}
				if (list.length > 14)
				{
					cbcCannedReportDTO.setEosinophil(list[14].toString());
				}
				if (list.length > 15)
				{
					cbcCannedReportDTO.setBasophil(list[15].toString());
				}
				if (list.length > 16)
				{
					cbcCannedReportDTO.setMetamyelocyte(list[16].toString());
				}
				if (list.length > 17)
				{
					cbcCannedReportDTO.setMyelocyte(list[17].toString());
				}
				if (list.length > 18)
				{
					cbcCannedReportDTO.setPromyelocyte(list[18].toString());
				}
				if (list.length > 19)
				{
					cbcCannedReportDTO.setPlateletEstimate(list[19].toString());
				}
				if (list.length > 20)
				{
					cbcCannedReportDTO.setPolychromasia(list[20].toString());
				}

				if (list.length > 21)
				{
					cbcCannedReportDTO.setAnisocytosis(list[21].toString());
				}
				if (list.length > 22)
				{
					cbcCannedReportDTO.setPoikilocytosis(list[22].toString());
				}
				if (list.length > 23)
				{
					cbcCannedReportDTO.setHeinzBodies(list[23].toString());
				}
				if (list.length > 24)
				{
					cbcCannedReportDTO.setRemarks(list[24].toString());
				}

				if (list.length > 25)
				{
					cbcCannedReportDTO.setAbsoluteNeutrophilSeg(list[25].toString());
				}
				if (list.length > 26)
				{
					cbcCannedReportDTO.setAbsoluteNeutrophilBand(list[26].toString());
				}
				if (list.length > 27)
				{
					cbcCannedReportDTO.setAbsoluteLymphocyte(list[27].toString());
				}
				if (list.length > 28)
				{
					cbcCannedReportDTO.setAbsoluteMonocyte(list[28].toString());
				}
				if (list.length > 29)
				{
					cbcCannedReportDTO.setAbsoluteEosinophil(list[29].toString());
				}
				if (list.length > 30)
				{
					cbcCannedReportDTO.setAbsoluteBasophil(list[30].toString());
				}
				if (list.length > 31)
				{
					cbcCannedReportDTO.setAbsoluteMetamyelocytes(list[31].toString());
				}
				if (list.length > 32)
				{
					cbcCannedReportDTO.setAbsolutePromyelocytes(list[32].toString());
				}
				if (list.length > 33)
				{
					cbcCannedReportDTO.setPlateletCount(list[33].toString());
				}
			}

			cbcCannedReports.add(cbcCannedReportDTO);
		}

		return cbcCannedReports;
	}

	@Override
	public List<?> getAnimalWeights(final String studyId)
	{
		Query query = entityManager.createQuery("SELECT a.id,a.weight FROM Animals a,Study s JOIN s.animals sa WHERE sa.id=a.id AND s.id=?");
		query.setParameter(1, Long.parseLong(studyId));
		List<Object[]> historyList = query.getResultList();
		List<AnimalWeightReportsDTO> animalWeightReports = new ArrayList<AnimalWeightReportsDTO>();

		for (Object[] list : historyList)
		{
			AnimalWeightReportsDTO animalWeightReportsDTO = new AnimalWeightReportsDTO();
			if (list != null && list.length > 0)
			{
				if (list[0] instanceof Long)
				{
					animalWeightReportsDTO.setAnimalId((Long) list[0]);
				}

			}

			if (list != null && list.length > 1)
			{
				animalWeightReportsDTO.setWeights(list[1].toString());
			}
			animalWeightReports.add(animalWeightReportsDTO);
		}

		return animalWeightReports;
	}

	@Override
	public List<?> getJobCodeDetails(String jobCode, Date fromDate, Date toDate)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(toDate);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DATE);
		calendar.set(year, month, day, 23, 59, 59);
		Date date = calendar.getTime();

		Query query = entityManager
				.createQuery("SELECT animalId,addedDate FROM AnimalHistory WHERE contractNumber=? AND addedDate BETWEEN ? AND ? ORDER BY addedDate ASC");

		query.setParameter(1, jobCode);
		query.setParameter(2, fromDate);
		query.setParameter(3, date);
		List<Object[]> historyList = query.getResultList();

		Map<String, List<Date>> dateDetails = new HashMap<String, List<Date>>();

		for (Object[] list : historyList)
		{

			if (list != null && list.length > 1)
			{

				if (dateDetails.containsKey(list[0].toString()))
				{
					dateDetails.get(list[0].toString()).add((Date) list[1]);
				} else
				{
					List<Date> dateList = new ArrayList<Date>();
					dateList.add((Date) list[1]);
					dateDetails.put(list[0].toString(), dateList);
				}

			}

		}

		List<AnimalJobCodeDetailReportsDTO> animalJobCodeDetailReports = new ArrayList<AnimalJobCodeDetailReportsDTO>();

		if (!dateDetails.isEmpty())
		{
			for (Map.Entry<String, List<Date>> map : dateDetails.entrySet())
			{
				AnimalJobCodeDetailReportsDTO animalJobCodeDetailReportsDTO = new AnimalJobCodeDetailReportsDTO();
				animalJobCodeDetailReportsDTO.setAnimalId(map.getKey());
				List<Date> dateValues = map.getValue();
				if (null != dateValues)
				{
					animalJobCodeDetailReportsDTO.setStartDate(dateValues.get(0));
					animalJobCodeDetailReportsDTO.setEndDate(dateValues.get(dateValues.size() - 1));

					Calendar calendarValue = Calendar.getInstance();
					calendarValue.setTime(animalJobCodeDetailReportsDTO.getEndDate());
					int yearValue = calendarValue.get(Calendar.YEAR);
					int monthValue = calendarValue.get(Calendar.MONTH);
					int dayValue = calendarValue.get(Calendar.DATE);
					calendarValue.set(yearValue, monthValue, dayValue, 23, 59, 59);
					Date dateValue = calendarValue.getTime();

					Calendar calendarValueFrom = Calendar.getInstance();
					calendarValueFrom.setTime(animalJobCodeDetailReportsDTO.getStartDate());
					int yearValueFrom = calendarValueFrom.get(Calendar.YEAR);
					int monthValueFrom = calendarValueFrom.get(Calendar.MONTH);
					int dayValueFrom = calendarValueFrom.get(Calendar.DATE);
					calendarValueFrom.set(yearValueFrom, monthValueFrom, dayValueFrom, 0, 0, 0);
					Date dateValueFrom = calendarValueFrom.getTime();

					long days = dateValue.getTime() - dateValueFrom.getTime();

					animalJobCodeDetailReportsDTO.setDays(TimeUnit.DAYS.convert(days, TimeUnit.MILLISECONDS));
				}
				animalJobCodeDetailReports.add(animalJobCodeDetailReportsDTO);
			}
		}

		return animalJobCodeDetailReports;
	}

	@Override
	public List<?> getAnimalTreatmentDetails(String animalId)
	{
		return null;
	}

	@Override
	public List<?> getRhesusAnimalDetails()
	{
		return null;
	}
}
