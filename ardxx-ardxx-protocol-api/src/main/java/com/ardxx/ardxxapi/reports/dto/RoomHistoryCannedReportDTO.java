package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;
import java.util.Date;

public class RoomHistoryCannedReportDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String room;
	private Date dateEntered;
	private Date dateExit;
	public String getRoom()
	{
		return room;
	}
	public void setRoom(String room)
	{
		this.room = room;
	}
	public Date getDateEntered()
	{
		return dateEntered;
	}
	public void setDateEntered(Date dateEntered)
	{
		this.dateEntered = dateEntered;
	}
	public Date getDateExit()
	{
		return dateExit;
	}
	public void setDateExit(Date dateExit)
	{
		this.dateExit = dateExit;
	}
	
	
	
}
