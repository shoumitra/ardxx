package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;
import java.util.Date;

public class AnimalJobCodeDetailReportsDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String animalId;
	private long days;
	private Date startDate;
	private Date endDate;

	public String getAnimalId()
	{
		return animalId;
	}

	public void setAnimalId(String animalId)
	{
		this.animalId = animalId;
	}

	public long getDays()
	{
		return days;
	}

	public void setDays(long days)
	{
		this.days = days;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

}
