package com.ardxx.ardxxapi.reports.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Raj
 *
 */
public class ReportDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String table;
	private List<String> columns;
	private String where;
	private List<WhereClause> whereClause;

	public String getTable()
	{
		return table;
	}

	public void setTable(String table)
	{
		this.table = table;
	}

	public List<String> getColumns()
	{
		return columns;
	}

	public void setColumns(List<String> columns)
	{
		this.columns = columns;
	}

	public String getWhere()
	{
		return where;
	}

	public void setWhere(String where)
	{
		this.where = where;
	}
	
	@Override
	public String toString()
	{
		return this.table;
	}
	
	public List<WhereClause> getWhereClause()
	{
		return whereClause;
	}

	public void setWhereClause(List<WhereClause> whereClause)
	{
		this.whereClause = whereClause;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return (this.table.equalsIgnoreCase(((ReportDTO) obj).table));
	}



}
