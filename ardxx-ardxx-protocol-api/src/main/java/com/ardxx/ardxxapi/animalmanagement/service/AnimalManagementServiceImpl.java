package com.ardxx.ardxxapi.animalmanagement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ardxx.ardxxapi.animalmanagement.controller.validator.Headers;
import com.ardxx.ardxxapi.animalmanagement.dao.AnimalHistoryRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.AnimalTestRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.AnimalTestResultRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.AnimalsRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.ELISATestRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.ImportFileHistoryDetailsRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.ImportFileHistoryRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.MamuListRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.MamuTestRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.SpeciesDetailRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.SpeciesRepository;
import com.ardxx.ardxxapi.animalmanagement.dao.VirusRepository;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalStatus;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTest;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTestResults;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.ELISATest;
import com.ardxx.ardxxapi.animalmanagement.domain.Format;
import com.ardxx.ardxxapi.animalmanagement.domain.ImportFileHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.ImportFileHistoryDetails;
import com.ardxx.ardxxapi.animalmanagement.domain.MamuList;
import com.ardxx.ardxxapi.animalmanagement.domain.MamuTest;
import com.ardxx.ardxxapi.animalmanagement.domain.Species;
import com.ardxx.ardxxapi.animalmanagement.domain.SpeciesDetail;
import com.ardxx.ardxxapi.animalmanagement.domain.Virus;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalTestResultDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ELISATestDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.MamuTestDTO;
import com.ardxx.ardxxapi.common.ARDXXUtils;
import com.tn.security.basic.domain.User;
import com.tn.security.basic.repository.UserRepository;

@Service
public class AnimalManagementServiceImpl implements AnimalManagementService
{

	@Autowired
	private AnimalsRepository animalsRepository;

	@Autowired
	private ImportFileHistoryRepository importFileHistoryRepository;

	@Autowired
	private ImportFileHistoryDetailsRepository importFileHistoryDetailsRepository;

	@Autowired
	private AnimalTestResultRepository animalTestResultRepository;

	@Autowired
	private AnimalTestRepository animalTestRepository;
	
	@Autowired
	private SpeciesRepository speciesDomainRepository;

	@Autowired
	private VirusRepository virusRepository;

	@Autowired
	private MamuListRepository mamuListRepository;

	@Autowired
	private ELISATestRepository eLISATestRepository;

	@Autowired
	private MamuTestRepository mamuTestRepository;

	@Autowired
	private SpeciesDetailRepository speciesDetailRepository;
	
	@Autowired
	private AnimalHistoryRepository animalHistoryRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public String uploadAnimalDetails(List<Animals> animalsList)
	{
		try
		{
			animalsRepository.save(animalsList);
			return "Successfully uploaded";
		} catch (Exception e)
		{
			e.printStackTrace();
			return "Error while upload file";
		}

	}

	@Override
	public void saveAnimalList(List<String[]> responseList, Headers headers)
	{
		Animals animals = null;
		for (String[] rows : responseList)
		{

			if (rows[rows.length - 1].equals("Success"))
			{
				try
				{

					animals = toAnimals(rows, headers);
					animalsRepository.save(animals);
				} catch (Exception exception)
				{
					exception.printStackTrace();
				}
			}
		}

	}

	private Animals toAnimals(String[] row, Headers headers)
	{
		Animals animals = new Animals();
		String animalId = getRowValue(row, headers.getID());
		String animalOldId = getRowValue(row, headers.getOLDID());
		if (ARDXXUtils.isEmpty(animalId) && !ARDXXUtils.isEmpty(animalOldId))
		{
			animalId = animalOldId;
		}

		if (ARDXXUtils.isEmpty(animalId) && ARDXXUtils.isEmpty(animalOldId))
		{
			animalId = ARDXXUtils.ANIMAL_ID_PREFIX+animalsRepository.getNextAnimalId();
			animalOldId = animalId;

		}
		animals.setAnimalId(animalId);
		animals.setOldId(animalOldId);
		animals.setSpecies(getRowValue(row, headers.getSPECIES()));
		animals.setSex(getRowValue(row, headers.getSEX()));
//		animals.setAgeCategory(getRowValue(row, headers.getAGECATEGORY()));
		animals.setClient(getRowValue(row, headers.getCLIENT()));
		

		Date birthDate = ARDXXUtils.dateFormat(getRowValue(row, headers.getDOB()), ARDXXUtils.DATE_FORMAT);
		animals.setDateOfBirth(null == birthDate ? null : birthDate);
		animals.setSource(getRowValue(row, headers.getSOURCE()));
		Date recievedDate = ARDXXUtils.dateFormat(getRowValue(row, headers.getRECEIVEDDATE()), ARDXXUtils.DATE_FORMAT);
		animals.setRecievedDate(null == recievedDate ? null : recievedDate);
		Date departedDate = ARDXXUtils.dateFormat(getRowValue(row, headers.getDEPARTEDDATE()), ARDXXUtils.DATE_FORMAT);
		animals.setDepartedDate(null == departedDate ? null : departedDate);
		animals.setRoomNum(getRowValue(row, headers.getROOMNUM()));
//		animals.setVirus(getRowValue(row, headers.getVIRUS()));
		animals.setWeight(getRowValue(row, headers.getWEIGHT()));
		animals.setOrigin(getRowValue(row, headers.getORIGIN()));

		animals.setStatus(AnimalStatus.HOLDING.name());

		return animals;
	}

	@Override
	public String[] saveImportHistory(List<String[]> responseList, String format, String uploadedBy, Headers headers, String header)
	{

		int numberOfSuccess = 0;
		String status = "";
		String[] response = new String[2];
		Animals animal = null;
		ImportFileHistory importFileHistory = new ImportFileHistory();

		importFileHistory.setFormat(format);
		importFileHistory.setUploadedBy(uploadedBy);
		importFileHistory.setUploadedDate(new Date());

		List<ImportFileHistoryDetails> importFileHistoryDetailsList = new ArrayList<ImportFileHistoryDetails>();

		if (Format.ANIMAL.name().equalsIgnoreCase(format))
		{
			for (String[] fields : responseList)
			{
				ImportFileHistoryDetails importFileHistoryDetails = new ImportFileHistoryDetails();
				if (fields[fields.length - 1].equalsIgnoreCase("Success"))
				{
					if(headers.getID() != -1)
					{
						animal = animalsRepository.findByAnimalId(fields[headers.getID()]);
					}else if(headers.getOLDID() != -1)
					{
						animal = animalsRepository.findByAnimalId(fields[headers.getOLDID()]);
					}
					
					if (animal == null)
					{
						numberOfSuccess++;
					} else
					{
						fields[fields.length - 1] = "Animal id already exists.";
					}

				}
				importFileHistoryDetails.setRemarks(fields[fields.length - 1]);
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < fields.length - 1; i++)
				{
					builder.append(fields[i]).append(",");
				}
				importFileHistoryDetails.setRawData(builder.toString());

				importFileHistoryDetails = importFileHistoryDetailsRepository.save(importFileHistoryDetails);

				importFileHistoryDetailsList.add(importFileHistoryDetails);
			}

			importFileHistory.setImportFileDetails(importFileHistoryDetailsList);

			importFileHistory.setHeader(header+",REMARKS");

		} else if (Format.CBC.name().equalsIgnoreCase(format))
		{
			for (int i = 0; i < responseList.size(); i++)
			{
				ImportFileHistoryDetails importFileHistoryDetails = new ImportFileHistoryDetails();
				if (responseList.get(i)[responseList.get(i).length - 1].equalsIgnoreCase("Success"))
				{
					if (i > 3)
					{
						animal = animalsRepository.findByAnimalId(responseList.get(i)[headers.getAnimal_Id()]);
						if (animal == null)
						{
							responseList.get(i)[responseList.get(i).length - 1] = "Animal ID doesnt exists";
						} else
						{
							numberOfSuccess++;
						}
					} else
					{
						numberOfSuccess++;
					}
				}
				importFileHistoryDetails.setRemarks(responseList.get(i)[responseList.get(i).length - 1]);
				StringBuilder builder = new StringBuilder();

				for (int k = 0; k < responseList.get(i).length - 1; k++)
				{
					builder.append(responseList.get(i)[k]).append(",");
				}
				importFileHistoryDetails.setRawData(builder.toString());

				importFileHistoryDetails = importFileHistoryDetailsRepository.save(importFileHistoryDetails);

				importFileHistoryDetailsList.add(importFileHistoryDetails);
			}

			importFileHistory.setImportFileDetails(importFileHistoryDetailsList);
			importFileHistory.setHeader(header+",REMARKS");
		}

		if (responseList.size() == numberOfSuccess)
		{
			status = "FULL SUCCESS";
		} else if (numberOfSuccess == 0)
		{
			status = "FULL FAILURE";
		} else
		{
			status = "PARTIAL SUCCESS";
		}

		importFileHistory.setStatus(status);

		importFileHistory = importFileHistoryRepository.save(importFileHistory);

		response[0] = String.valueOf(importFileHistory.getId());
		response[1] = importFileHistory.getStatus();
		return response;
	}

	@Override
	public Animals getAnimal(Long id)
	{
		return animalsRepository.findOne(id);
	}

	@Override
	public Iterable<Animals> getAnimalsList()
	{
		return animalsRepository.findAll();
	}

	@Override
	public Iterable<ImportFileHistory> getImportFileHistory()
	{
		return importFileHistoryRepository.findAll();
	}

	@Override
	public List<String> getResponseFile(String id)
	{
		List<String> reposneList = new ArrayList<String>();

		ImportFileHistory importFileHistory = importFileHistoryRepository.findOne(Long.parseLong(id));

		if (importFileHistory != null)
		{

			List<ImportFileHistoryDetails> importFileHistoryDetails = importFileHistory.getImportFileDetails();
			if (importFileHistory.getFormat().equalsIgnoreCase(Format.ANIMAL.name()))
			{
				reposneList.add(importFileHistory.getHeader());
			}

			int i = 0;

			for (ImportFileHistoryDetails historyDetails : importFileHistoryDetails)
			{

				StringBuilder builder = new StringBuilder(historyDetails.getRawData()).append(historyDetails.getRemarks());
				if (i == 4)
				{
					if (importFileHistory.getFormat().equalsIgnoreCase(Format.CBC.name()))
					{
						reposneList.add(importFileHistory.getHeader());
					}
				}
				reposneList.add(builder.toString());

				i++;
			}

		}

		return reposneList;
	}

	@Override
	public Animals createAnimal(Animals animals)
	{
		User user = null;
		
		if(ARDXXUtils.isEmpty(animals.getOldId()) && ARDXXUtils.isEmpty(animals.getAnimalId()))
		{
			animals.setAnimalId(ARDXXUtils.ANIMAL_ID_PREFIX + animalsRepository.getNextAnimalId());
		}
		
		if(!ARDXXUtils.isEmpty(animals.getOldId()) && ARDXXUtils.isEmpty(animals.getAnimalId()))
		{
			animals.setAnimalId(animals.getOldId());
		}
		
		if(ARDXXUtils.isEmpty(animals.getStatus()))
		{
			animals.setStatus(AnimalStatus.HOLDING.name());
		}
		
		if(animals.getVetTech() != null && animals.getVetTech().getId() != null)
		{
			user = userRepository.findOne(animals.getVetTech().getId());
			animals.setVetTech(user);
		}
		return animalsRepository.save(animals);
	}

	@Override
	public Animals updateAnimal(Animals animals)
	{
		return animalsRepository.save(animals);
	}

	@Override
	public void saveAnimalTestResults(List<String[]> responseList, Headers headers)
	{

		Animals animal;

		AnimalTest animalTest = new AnimalTest();
		animalTest.setAntrimAccountNumber(responseList.get(0)[0].split(":")[1]);
		animalTest.setFileHeader(responseList.get(3)[0].split(":")[1]);
		animalTest.setStudyDirector(responseList.get(2)[0].split(":")[1]);
		animalTest.setStudyId(responseList.get(1)[0].split(":")[1]);

		List<AnimalTestResults> animalTestResultsList = new ArrayList<AnimalTestResults>();

		for (int i = 4; i < responseList.size(); i++)
		{

			if (responseList.get(i)[responseList.get(i).length - 1].equalsIgnoreCase("Success"))
			{
				try
				{
					animal = animalsRepository.findByAnimalId(responseList.get(i)[headers.getAnimal_Id()]);
					if (animal != null)
					{
						AnimalTestResults animalTestResults = toAnimalTestResults(responseList.get(i), headers);
						animalTestResults.setAnimalId(animal);
						animalTestResults = animalTestResultRepository.save(animalTestResults);
						animalTestResultsList.add(animalTestResults);
					}

				} catch (Exception exception)
				{
					exception.printStackTrace();
				}
			}
		}

		animalTest.setAnimalTestResults(animalTestResultsList);
		animalTestRepository.save(animalTest);

	}

	private AnimalTestResults toAnimalTestResults(String[] rows, Headers headers)
	{
		int i = 0;
		AnimalTestResults animalTestResults = new AnimalTestResults();
		animalTestResults.setAccessionNumber(getRowValue(rows, headers.getAccession_Number()));
		animalTestResults.setGroupNumber(getRowValue(rows, headers.getGroup_Number()));
		i++; // To skip animal id row count
		animalTestResults.setSex(getRowValue(rows, headers.getSEX()));
		animalTestResults.setSpecies(getRowValue(rows, headers.getSPECIES()));
		animalTestResults.setAge(getRowValue(rows, headers.getAge()));
		animalTestResults.setBreed(getRowValue(rows, headers.getBreed()));
		Date collectedDate = ARDXXUtils.dateFormat(getRowValue(rows, headers.getCollected_Date()), ARDXXUtils.DATE_FORMAT);
		animalTestResults.setCollectedDate(collectedDate);
		animalTestResults.setAgRatio(getRowValue(rows, headers.getA_G_RATIO()));
		animalTestResults.setAbsoluteBasophil(getRowValue(rows, headers.getABSOLUTE_BASOPHIL()));
		animalTestResults.setAbsoluteEosinophil(getRowValue(rows, headers.getABSOLUTE_EOSINOPHIL()));
		animalTestResults.setAbsoluteLymphocyte(getRowValue(rows, headers.getABSOLUTE_LYMPHOCYTE()));
		animalTestResults.setAbsoluteMetamyelocytes(getRowValue(rows, headers.getABSOLUTE_METAMYELOCYTES()));
		animalTestResults.setAbsoluteMonocyte(getRowValue(rows, headers.getABSOLUTE_MONOCYTE()));
		animalTestResults.setAbsoluteNeutrophilBand(getRowValue(rows, headers.getABSOLUTE_NEUTROPHIL_BAND()));
		animalTestResults.setAbsoluteNeutrophilSeg(getRowValue(rows, headers.getABSOLUTE_NEUTROPHIL_SEG()));
		animalTestResults.setAbsolutePromyelocytes(getRowValue(rows, headers.getABSOLUTE_PROMYELOCYTES()));
		animalTestResults.setAlbumin(getRowValue(rows, headers.getALBUMIN()));
		animalTestResults.setAlkalinePhosphatase(getRowValue(rows, headers.getALKALINE_PHOSPHATASE()));
		animalTestResults.setAnisocytosis(getRowValue(rows, headers.getANISOCYTOSIS()));
		animalTestResults.setBcRatio(getRowValue(rows, headers.getB_C_RATIO()));
		animalTestResults.setBasophil(getRowValue(rows, headers.getBASOPHIL()));
		animalTestResults.setBicarbonate(getRowValue(rows, headers.getBICARBONATE()));
		animalTestResults.setBun(getRowValue(rows, headers.getBUN()));
		animalTestResults.setCalcium(getRowValue(rows, headers.getCALCIUM()));
		animalTestResults.setChloride(getRowValue(rows, headers.getCHLORIDE()));
		animalTestResults.setCholesterol(getRowValue(rows, headers.getCHOLESTEROL()));
		animalTestResults.setCpk(getRowValue(rows, headers.getCPK()));
		animalTestResults.setCreatinine(getRowValue(rows, headers.getCREATININE()));
		animalTestResults.setDirectBilirubin(getRowValue(rows, headers.getDIRECT_BILIRUBIN()));
		animalTestResults.setEosinophil(getRowValue(rows, headers.getEOSINOPHIL()));
		animalTestResults.setGlobulin(getRowValue(rows, headers.getGLOBULIN()));
		animalTestResults.setGlucose(getRowValue(rows, headers.getGLUCOSE()));
		animalTestResults.setHct(getRowValue(rows, headers.getHCT()));
		animalTestResults.setHeinzBodies(getRowValue(rows, headers.getHEINZ_BODIES()));
		animalTestResults.setHemolysisIndex(getRowValue(rows, headers.getHEMOLYSIS_INDEX()));
		animalTestResults.setHgb(getRowValue(rows, headers.getHGB()));
		animalTestResults.setIndirectBilirubin(getRowValue(rows, headers.getINDIRECT_BILIRUBIN()));
		animalTestResults.setLipemiaIndex(getRowValue(rows, headers.getLIPEMIA_INDEX()));
		animalTestResults.setLymphocyte(getRowValue(rows, headers.getLYMPHOCYTE()));
		animalTestResults.setMch(getRowValue(rows, headers.getMCH()));
		animalTestResults.setMchc(getRowValue(rows, headers.getMCHC()));
		animalTestResults.setMcv(getRowValue(rows, headers.getMCV()));
		animalTestResults.setMetamyelocyte(getRowValue(rows, headers.getMETAMYELOCYTE()));
		animalTestResults.setMonocyte(getRowValue(rows, headers.getMONOCYTE()));
		animalTestResults.setMyelocyte(getRowValue(rows, headers.getMYELOCYTE()));
		animalTestResults.setNakRatio(getRowValue(rows, headers.getNA_K_RATIO()));
		animalTestResults.setNeutrophilBand(getRowValue(rows, headers.getNEUTROPHIL_BAND()));
		animalTestResults.setNeutrophilSeg(getRowValue(rows, headers.getNEUTROPHIL_SEG()));
		animalTestResults.setNrbc(getRowValue(rows, headers.getNRBC()));
		animalTestResults.setPhosphorus(getRowValue(rows, headers.getPHOSPHORUS()));
		animalTestResults.setPlateletCount(getRowValue(rows, headers.getPLATELET_COUNT()));
		animalTestResults.setPlateletEstimate(getRowValue(rows, headers.getPLATELET_ESTIMATE()));
		animalTestResults.setPoikilocytosis(getRowValue(rows, headers.getPOIKILOCYTOSIS()));
		animalTestResults.setPolychromasia(getRowValue(rows, headers.getPOLYCHROMASIA()));
		animalTestResults.setPotassium(getRowValue(rows, headers.getPOTASSIUM()));
		animalTestResults.setPromyelocyte(getRowValue(rows, headers.getPROMYELOCYTE()));
		animalTestResults.setRbc(getRowValue(rows, headers.getRBC()));
		animalTestResults.setRemarks(getRowValue(rows, headers.getREMARKS()));
		animalTestResults.setSgot(getRowValue(rows, headers.getSGOT_AST()));
		animalTestResults.setSgpt(getRowValue(rows, headers.getSGPT_ALT()));
		animalTestResults.setSodium(getRowValue(rows, headers.getSODIUM()));
		animalTestResults.setTotalBilirubin(getRowValue(rows, headers.getTOTAL_BILIRUBIN()));
		animalTestResults.setTotalProtein(getRowValue(rows, headers.getTOTAL_PROTEIN()));
		animalTestResults.setWbc(getRowValue(rows, headers.getWBC()));

		return animalTestResults;
	}

	private String getRowValue(String[] rows, int index)
	{
		if (index == -1)
		{
			return null;
		} else
		{
			return rows[index];
		}

	}

	@Override
	public Iterable<AnimalTest> getAllAnimalTests()
	{
		return animalTestRepository.findAll();

	}

	@Override
	public AnimalTest getAnimalTest(Long animalTestId)
	{
		return animalTestRepository.findOne(animalTestId);
	}

	@Override
	public Iterable<AnimalTestResults> getAnimalTestResults()
	{
		return animalTestResultRepository.findAll();

	}

	@Override
	public Iterable<AnimalTestResults> getAnimalTestResultsForDateRange(Date startDate, Date endDate)
	{
		return animalTestResultRepository.findByCollectedDate(startDate, endDate);
	}

	@Override
	public Iterable<SpeciesDetail> getSpeciesRangeDetails()
	{
		return speciesDetailRepository.findAll();
	}

	@Override
	public SpeciesDetail getSpeciesRangeDetails(Long id)
	{
		return speciesDetailRepository.findOne(id);
	}

	@Override
	public Iterable<AnimalTestResults> getAnimalTestResultsForAnimal(Long animalId)
	{
		return animalTestResultRepository.findByAnimalIdId(animalId);
	}

	@Override
	public AnimalTestResults getAnimalTestResultById(Long animalTestResultId)
	{
		return animalTestResultRepository.findOne(animalTestResultId);
	}

	@Override
	public ELISATest addElisa(ELISATestDTO elisaTestDto)
	{
		ELISATest elisaTest = new ELISATest();
		elisaTest.setCollectedDate(elisaTestDto.getCollectedDate());
		elisaTest.setResult(elisaTestDto.getResult());
		elisaTest.setValue(elisaTestDto.getValue());

		Virus virus = virusRepository.findOne(elisaTestDto.getVirus());
		elisaTest.setVirus(virus);

		Animals animals = animalsRepository.findOne(elisaTestDto.getAnimals());
		elisaTest.setAnimals(animals);

		elisaTest = eLISATestRepository.save(elisaTest);

		return elisaTest;
	}

	@Override
	public Iterable<Virus> getAllViruses()
	{
		return virusRepository.findAll();

	}

	@Override
	public Iterable<MamuList> getAllMamuList()
	{
		return mamuListRepository.findAll();
	}

	@Override
	public MamuTest addMamu(MamuTestDTO mamuTestDto)
	{
		MamuTest mamuTest = new MamuTest();
		mamuTest.setCollectedDate(mamuTestDto.getCollectedDate());
		mamuTest.setResult(mamuTestDto.getResult());
		mamuTest.setValue(mamuTestDto.getValue());

		MamuList mamuList = mamuListRepository.findOne(mamuTestDto.getMamu());
		mamuTest.setMamu(mamuList);

		Animals animals = animalsRepository.findOne(mamuTestDto.getAnimals());
		mamuTest.setAnimals(animals);

		mamuTest = mamuTestRepository.save(mamuTest);

		return mamuTest;
	}

	@Override
	public Iterable<ELISATest> getElisaTestByAnimalId(String animalId)
	{
		return eLISATestRepository.getElisaTestByAnimalId(animalId);
	}

	@Override
	public Iterable<MamuTest> getMamuTestByAnimalId(String animalId)
	{
		return mamuTestRepository.getMamuTestByAnimalId(animalId);
	}

	@Override
	public ELISATest updateElisa(ELISATestDTO elisaTestDto)
	{

		ELISATest elisaTest = eLISATestRepository.findOne(elisaTestDto.getId());
		if (elisaTest == null)
		{
			return null;
		} else
		{
			elisaTest.setCollectedDate(elisaTestDto.getCollectedDate());
			elisaTest.setResult(elisaTestDto.getResult());
			elisaTest.setValue(elisaTestDto.getValue());

			Virus virus = virusRepository.findOne(elisaTestDto.getVirus());
			elisaTest.setVirus(virus);

			Animals animals = animalsRepository.findOne(elisaTestDto.getAnimals());
			elisaTest.setAnimals(animals);

			elisaTest = eLISATestRepository.save(elisaTest);

			return elisaTest;
		}

	}

	@Override
	public MamuTest updateMamu(MamuTestDTO mamuTestDto)
	{

		MamuTest mamuTest = mamuTestRepository.findOne(mamuTestDto.getId());
		if (mamuTest == null)
		{
			return null;
		} else
		{
			mamuTest.setCollectedDate(mamuTestDto.getCollectedDate());
			mamuTest.setResult(mamuTestDto.getResult());
			mamuTest.setValue(mamuTestDto.getValue());

			MamuList mamuList = mamuListRepository.findOne(mamuTestDto.getMamu());
			mamuTest.setMamu(mamuList);

			Animals animals = animalsRepository.findOne(mamuTestDto.getAnimals());
			mamuTest.setAnimals(animals);

			mamuTest = mamuTestRepository.save(mamuTest);

			return mamuTest;
		}

	}

	@Override
	public ELISATest deleteElisa(Long id)
	{
		eLISATestRepository.delete(id);

		return null;
	}

	@Override
	public MamuTest deleteMamu(Long id)
	{
		mamuTestRepository.delete(id);
		return null;
	}

	@Override
	public Iterable<AnimalTestResults> getCBCTestResults(AnimalTestResultDTO animalTestResultDTO)
	{
		return animalTestRepository.getCBCTestResults(animalTestResultDTO);
	}

	@Override
	public Iterable<Species> getAllSpecies()
	{
		return speciesDomainRepository.findAll();
	}
	
	@Override
	public void saveAnimalHistory(AnimalHistory animalHistory)
	{
		animalHistoryRepository.save(animalHistory);
	}

	@Override
	public Iterable<AnimalHistory> getAnimalHistory(Long animalId)
	{
		 return animalHistoryRepository.findByAnimalTableId(animalId);
	}

	@Override
	public List<Animals> getAnimalByStatus(String status)
	{
		return animalsRepository.findByStatus(status);
	}
	
}
