package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SpeciesDetail implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String speciesName;

	@ElementCollection
	private List<SpeciesRangeDetail> speciesRangeDetails;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getSpeciesName()
	{
		return speciesName;
	}

	public void setSpeciesName(String speciesName)
	{
		this.speciesName = speciesName;
	}

	public List<SpeciesRangeDetail> getSpeciesRangeDetails()
	{
		return speciesRangeDetails;
	}

	public void setSpeciesRangeDetails(List<SpeciesRangeDetail> speciesRangeDetails)
	{
		this.speciesRangeDetails = speciesRangeDetails;
	}

}
