package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.domain.SpeciesDetail;

@Repository
public interface SpeciesDetailRepository extends PagingAndSortingRepository<SpeciesDetail, Long>
{
}
