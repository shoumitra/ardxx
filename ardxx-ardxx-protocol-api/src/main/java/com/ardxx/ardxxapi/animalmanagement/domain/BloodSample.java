package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BloodSample implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String tubeType;
	private String quantity;
	private String multipleTubes;
	private String unit;
	private String notes;

	@ElementCollection
	List<ProcedureTest> procedureTests;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getTubeType()
	{
		return tubeType;
	}

	public void setTubeType(String tubeType)
	{
		this.tubeType = tubeType;
	}

	public String getQuantity()
	{
		return quantity;
	}

	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

	public String getMultipleTubes()
	{
		return multipleTubes;
	}

	public void setMultipleTubes(String multipleTubes)
	{
		this.multipleTubes = multipleTubes;
	}

	public List<ProcedureTest> getProcedureTests()
	{
		return procedureTests;
	}

	public void setProcedureTests(List<ProcedureTest> procedureTests)
	{
		this.procedureTests = procedureTests;
	}

	public String getUnit()
	{
		return unit;
	}

	public void setUnit(String unit)
	{
		this.unit = unit;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

}
