package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Species implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String speciesName;
	
	public String getSpeciesName()
	{
		return speciesName;
	}
	public void setSpeciesName(String speciesName)
	{
		this.speciesName = speciesName;
	}
	
	
}
