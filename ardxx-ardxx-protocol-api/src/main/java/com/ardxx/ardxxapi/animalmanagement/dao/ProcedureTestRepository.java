package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTest;

public interface ProcedureTestRepository extends PagingAndSortingRepository<ProcedureTest, Long>
{

}
