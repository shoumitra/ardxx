package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;

public class VolumeDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String substanceName;


	public String getSubstanceName()
	{
		return substanceName;
	}

	public void setSubstanceName(String substanceName)
	{
		this.substanceName = substanceName;
	}

}
