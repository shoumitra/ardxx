package com.ardxx.ardxxapi.animalmanagement.controller.validator;

import java.util.ArrayList;
import java.util.List;

import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.Format;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalRequestUploadDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ELISATestDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.MamuTestDTO;
import com.ardxx.ardxxapi.common.ARDXXUtils;
import com.ardxx.ardxxapi.common.Messages;

public class AnimalsValidator
{
	
	public List<String[]> validateAnimals(AnimalRequestUploadDTO animalRequestUpload,Headers headers,List<String> species)
	{

		List<String[]> responseList = new ArrayList<String[]>();
		if (Format.ANIMAL.name().equalsIgnoreCase(animalRequestUpload.getFormat()))
		{
			String header = animalRequestUpload.getAnimalsList().remove(0);
			int noOfColumns = header.split(",").length+1;
			for (String field : animalRequestUpload.getAnimalsList())
			{
				if (!ARDXXUtils.isEmpty(field))
				{
					String[] fields = new String[noOfColumns];
					int i = 0;
					for (String ss : field.split(","))
					{
						fields[i] = ss;
						i++;
					}
					StringBuilder message = new StringBuilder();
					String responseMesage = "";
					message = validateFields(message, fields, headers,species);
					if (message.length() == 0)
					{
						responseMesage = "Success";
					} else
					{
						responseMesage = message.toString();
					}
					fields[noOfColumns-1] = responseMesage;
					responseList.add(fields);
				}
			}
		} else if (Format.CBC.name().equalsIgnoreCase(animalRequestUpload.getFormat()))
		{
			String header = animalRequestUpload.getAnimalsList().remove(4);
			int noOfColumns = header.split(",").length + 1;
			int rowCount = 0;
			for (String field : animalRequestUpload.getAnimalsList())
			{
				if (!ARDXXUtils.isEmpty(field))
				{

					String[] fields = new String[noOfColumns];
					int i = 0;
					for (String ss : field.split(","))
					{
						fields[i] = ss;
						i++;
					}
					StringBuilder message = new StringBuilder();
					String responseMesage = "Success";

					if (rowCount > 4)
					{
						message = validateFieldsForTestResults(message, fields, headers);
					}

					if (message.length() == 0)
					{
						responseMesage = "Success";
					} else
					{
						responseMesage = message.toString();
					}

					fields[noOfColumns-1] = responseMesage;
					responseList.add(fields);

					rowCount++;
				}
			}
		}

		return responseList;
	}

	private StringBuilder validateFields(StringBuilder message, String[] fields,Headers header,List<String> species)
	{

		if (ARDXXUtils.isEmpty(fields[header.getSPECIES()]))
		{
			message.append(Messages.properties.getProperty(Messages.properties.getProperty("species.not.empty")+"."+ARDXXUtils.MESSAGE));
		} else if (!isValidEnum(fields[header.getSPECIES()].toLowerCase(),species))
		{
			message.append(Messages.properties.getProperty(Messages.properties.getProperty("species.not.valid")+"."+ARDXXUtils.MESSAGE));
		}

		if (ARDXXUtils.isEmpty(fields[header.getSEX()]))
		{
			message.append(Messages.properties.getProperty(Messages.properties.getProperty("sex.not.empty")+"."+ARDXXUtils.MESSAGE));
		}

		return message;
	}

	private StringBuilder validateFieldsForTestResults(StringBuilder message, String[] fields, Headers headers)
	{

		if (ARDXXUtils.isEmpty(fields[headers.getAnimal_Id()]))
		{
			message.append(Messages.properties.getProperty(Messages.properties.getProperty("animal.id.not.empty")+"."+ARDXXUtils.MESSAGE));
		}

		return message;
	}

	private boolean isValidEnum(String value,List<String> species)
	{
		for (String species2 : species)
		{
			if (species2.equalsIgnoreCase(value))
			{
				return true;
			}
		}
		
		return false;
	}

	public List<String> validateForAnimalCreate(Animals animals,List<String> species)
	{
		List<String> errorCode = new ArrayList<String>();
		/*if (ARDXXUtils.isEmpty(animals.getAnimalId()))
		{
			errorCode.add(Messages.properties.getProperty("animal.id.not.empty"));
		}*/

		if (ARDXXUtils.isEmpty(animals.getSpecies()))
		{
			errorCode.add(Messages.properties.getProperty("species.not.empty"));
		}
		else if(!isValidEnum(animals.getSpecies(), species))
		{
			errorCode.add(Messages.properties.getProperty("species.not.valid"));
		}

		if (ARDXXUtils.isEmpty(animals.getSex()))
		{
			errorCode.add(Messages.properties.getProperty("sex.not.empty"));
		}

		return errorCode;
	}

	public List<String> validateElisaTest(ELISATestDTO elisaTestDTO)
	{
		List<String> errorCode = new ArrayList<String>();

		if (ARDXXUtils.isEmpty(elisaTestDTO.getValue()))
		{
			errorCode.add(Messages.properties.getProperty("elisa.value.empty"));
		}
		
		if (ARDXXUtils.isEmpty(elisaTestDTO.getResult()))
		{
			errorCode.add(Messages.properties.getProperty("elisa.result.empty"));
		}

		if (elisaTestDTO.getCollectedDate() == null)
		{
			errorCode.add(Messages.properties.getProperty("elisa.collected.date.empty"));
		}
		
		if (elisaTestDTO.getVirus() == null)
		{
			errorCode.add(Messages.properties.getProperty("elisa.virus.empty"));
		}
		
		if (elisaTestDTO.getAnimals() == null)
		{
			errorCode.add(Messages.properties.getProperty("elisa.animals.empty"));
		}
		
		return errorCode;
	}
	
	public List<String> validateMamuTest(MamuTestDTO mamuTestDTO)
	{
		List<String> errorCode = new ArrayList<String>();

		if (ARDXXUtils.isEmpty(mamuTestDTO.getValue()))
		{
			errorCode.add(Messages.properties.getProperty("mamu.value.empty"));
		}
		
		if (ARDXXUtils.isEmpty(mamuTestDTO.getResult()))
		{
			errorCode.add(Messages.properties.getProperty("mamu.result.empty"));
		}

		if (mamuTestDTO.getCollectedDate() == null)
		{
			errorCode.add(Messages.properties.getProperty("mamu.collected.date.empty"));
		}
		
		if (mamuTestDTO.getMamu() == null)
		{
			errorCode.add(Messages.properties.getProperty("mamu.mamu.empty"));
		}
		
		if (mamuTestDTO.getAnimals() == null)
		{
			errorCode.add(Messages.properties.getProperty("mamu.animals.empty"));
		}
		
		return errorCode;
	}
}
