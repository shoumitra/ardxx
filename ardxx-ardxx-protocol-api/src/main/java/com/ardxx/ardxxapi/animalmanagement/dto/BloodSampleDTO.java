package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;

public class BloodSampleDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String tubeType;
	private String quantity;
	private String multipleTubes;
	private String quantityPerTube;
	private String unit;

	public String getTubeType()
	{
		return tubeType;
	}

	public void setTubeType(String tubeType)
	{
		this.tubeType = tubeType;
	}

	public String getQuantity()
	{
		return quantity;
	}

	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

	public String getMultipleTubes()
	{
		return multipleTubes;
	}

	public void setMultipleTubes(String multipleTubes)
	{
		this.multipleTubes = multipleTubes;
	}

	public String getQuantityPerTube()
	{
		return quantityPerTube;
	}

	public void setQuantityPerTube(String quantityPerTube)
	{
		this.quantityPerTube = quantityPerTube;
	}

	public String getUnit()
	{
		return unit;
	}

	public void setUnit(String unit)
	{
		this.unit = unit;
	}

}
