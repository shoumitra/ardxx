package com.ardxx.ardxxapi.animalmanagement.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Raj
 *
 */
public class MamuTestDTO implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private Date collectedDate;
	private String result;
	private String value;
	private Long mamu;
	private Long animals;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}
	
	public Date getCollectedDate()
	{
		return collectedDate;
	}

	public void setCollectedDate(Date collectedDate)
	{
		this.collectedDate = collectedDate;
	}

	public String getResult()
	{
		return result;
	}

	public void setResult(String result)
	{
		this.result = result;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public Long getAnimals()
	{
		return animals;
	}

	public void setAnimals(Long animals)
	{
		this.animals = animals;
	}

	public Long getMamu()
	{
		return mamu;
	}

	public void setMamu(Long mamu)
	{
		this.mamu = mamu;
	}

}
