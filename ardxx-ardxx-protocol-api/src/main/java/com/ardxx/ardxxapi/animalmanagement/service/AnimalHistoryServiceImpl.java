package com.ardxx.ardxxapi.animalmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ardxx.ardxxapi.animalmanagement.dao.AnimalHistoryRepository;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;

@Repository
public class AnimalHistoryServiceImpl
{

	@Autowired
	AnimalHistoryRepository animalHistoryRepository;
	
	public void saveAnimalHistory(AnimalHistory animalHistory)
	{
		animalHistoryRepository.save(animalHistory);
	}
}
