package com.ardxx.ardxxapi.animalmanagement.dao;

import org.springframework.beans.factory.annotation.Value;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.springframework.stereotype.Repository;

@Repository
public class AnimalsRepositoryImpl implements AnimalsRepositoryCustom
{
	@Value("${application.schema}")
	String schema;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(value = TxType.REQUIRED)
	public Long getNextAnimalId()
	{
		
//		entityManager.createQuery("").
		Query query = entityManager.createNativeQuery("SELECT Auto_increment FROM information_schema.tables WHERE table_name='animals' and table_schema = '"+schema+"'");
		Object object = query.getSingleResult();
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>.... "+object);
		return Long.parseLong(object.toString());
	}

	
}
