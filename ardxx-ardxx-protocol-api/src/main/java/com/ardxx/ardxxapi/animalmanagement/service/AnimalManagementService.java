package com.ardxx.ardxxapi.animalmanagement.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Configuration;

import com.ardxx.ardxxapi.animalmanagement.controller.validator.Headers;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTest;
import com.ardxx.ardxxapi.animalmanagement.domain.AnimalTestResults;
import com.ardxx.ardxxapi.animalmanagement.domain.Animals;
import com.ardxx.ardxxapi.animalmanagement.domain.ELISATest;
import com.ardxx.ardxxapi.animalmanagement.domain.ImportFileHistory;
import com.ardxx.ardxxapi.animalmanagement.domain.MamuList;
import com.ardxx.ardxxapi.animalmanagement.domain.MamuTest;
import com.ardxx.ardxxapi.animalmanagement.domain.SpeciesDetail;
import com.ardxx.ardxxapi.animalmanagement.domain.Species;
import com.ardxx.ardxxapi.animalmanagement.domain.Virus;
import com.ardxx.ardxxapi.animalmanagement.dto.AnimalTestResultDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.ELISATestDTO;
import com.ardxx.ardxxapi.animalmanagement.dto.MamuTestDTO;

/**
 * AnimalManagementService used to write business logic for Animal entity.
 * 
 * @author raj
 *
 */
@Configurable
public interface AnimalManagementService
{

	/**
	 * uploadAnimalDetails method upload the animal details.
	 * 
	 * @param animalsList
	 * @return
	 */
	String uploadAnimalDetails(List<Animals> animalsList);

	/**
	 * @param responseList
	 */
	void saveAnimalList(List<String[]> responseList,Headers headers);

	/**
	 * @param responseList
	 */
	String[] saveImportHistory(List<String[]> responseList, String format, String uploadedBy,Headers headers,String header);

	/**
	 * @param id
	 * @return
	 */
	Animals getAnimal(Long id);

	/**
	 * @return
	 */
	Iterable<Animals> getAnimalsList();

	Iterable<ImportFileHistory> getImportFileHistory();

	List<String> getResponseFile(String id);

	Animals createAnimal(Animals animals);

	Animals updateAnimal(Animals animals);

	void saveAnimalTestResults(List<String[]> responseList, Headers headers);

	Iterable<AnimalTest> getAllAnimalTests();

	AnimalTest getAnimalTest(Long animalTestId);

	Iterable<AnimalTestResults> getAnimalTestResults();

	Iterable<AnimalTestResults> getAnimalTestResultsForDateRange(Date startDate, Date endDate);

	Iterable<SpeciesDetail> getSpeciesRangeDetails();

	SpeciesDetail getSpeciesRangeDetails(Long id);
	Iterable<AnimalTestResults> getAnimalTestResultsForAnimal(Long animalId);

	AnimalTestResults getAnimalTestResultById(Long animalTestResultId);

	ELISATest addElisa(ELISATestDTO elisaTest);

	Iterable<Virus> getAllViruses();

	Iterable<MamuList> getAllMamuList();

	MamuTest addMamu(MamuTestDTO mamuTestDto);

	Iterable<ELISATest> getElisaTestByAnimalId(String animalId);

	Iterable<MamuTest> getMamuTestByAnimalId(String animalId);

	ELISATest updateElisa(ELISATestDTO elisaTestDto);

	MamuTest updateMamu(MamuTestDTO mamuTestDto);

	ELISATest deleteElisa(Long id);

	MamuTest deleteMamu(Long id);

	Iterable<AnimalTestResults> getCBCTestResults(AnimalTestResultDTO animalTestResultDTO);

	Iterable<Species> getAllSpecies();
	
	public void saveAnimalHistory(AnimalHistory animalHistory);

	Iterable<AnimalHistory> getAnimalHistory(Long animalId);

	List<Animals> getAnimalByStatus(String status);

}
