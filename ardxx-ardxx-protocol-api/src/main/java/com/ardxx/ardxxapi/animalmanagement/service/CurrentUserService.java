package com.ardxx.ardxxapi.animalmanagement.service;

import com.ardxx.ardxxapi.animalmanagement.domain.ProcedureTest;
import com.tn.security.basic.domain.CurrentUser;

public interface CurrentUserService
{
	public boolean canEditProcedureDetails(CurrentUser currentUser, ProcedureTest procedureTest);
}
