package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Inoculation implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String material;
	private String materialPrep;
	private String deliveryDevice;
	private String route;
	private String site;
	private String sitePrep;
	private float dosage;
	private String dosageUnit;
	private float volume;
	private String volumeUnit;
	private String notes;
	
	public long getId()
	{
		return id;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public String getMaterial()
	{
		return material;
	}
	public void setMaterial(String material)
	{
		this.material = material;
	}
	public String getMaterialPrep()
	{
		return materialPrep;
	}
	public void setMaterialPrep(String materialPrep)
	{
		this.materialPrep = materialPrep;
	}
	public String getDeliveryDevice()
	{
		return deliveryDevice;
	}
	public void setDeliveryDevice(String deliveryDevice)
	{
		this.deliveryDevice = deliveryDevice;
	}
	public String getRoute()
	{
		return route;
	}
	public void setRoute(String route)
	{
		this.route = route;
	}
	public String getSite()
	{
		return site;
	}
	public void setSite(String site)
	{
		this.site = site;
	}
	public String getSitePrep()
	{
		return sitePrep;
	}
	public void setSitePrep(String sitePrep)
	{
		this.sitePrep = sitePrep;
	}
	public float getDosage()
	{
		return dosage;
	}
	public void setDosage(float dosage)
	{
		this.dosage = dosage;
	}
	public String getDosageUnit()
	{
		return dosageUnit;
	}
	public void setDosageUnit(String dosageUnit)
	{
		this.dosageUnit = dosageUnit;
	}
	public float getVolume()
	{
		return volume;
	}
	public void setVolume(float volume)
	{
		this.volume = volume;
	}
	public String getVolumeUnit()
	{
		return volumeUnit;
	}
	public void setVolumeUnit(String volumeUnit)
	{
		this.volumeUnit = volumeUnit;
	}
	public String getNotes()
	{
		return notes;
	}
	public void setNotes(String notes)
	{
		this.notes = notes;
	}
	
	
	
}
