package com.ardxx.ardxxapi.animalmanagement.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.springframework.data.rest.core.annotation.RestResource;

import com.ardxx.ardxxapi.animalmanagement.dao.AnimalsEventHandler;
import com.tn.security.basic.domain.User;

/**
 * 
 * @author Raj
 *
 */
@Entity
@EntityListeners(AnimalsEventHandler.class)
public class Animals implements Serializable
{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String animalId;

	private String oldId;
	private String species;
	private String sex;
	private String client;
	private Date dateOfBirth;
	private String source;
	private Date recievedDate;
	private Date departedDate;
	private String roomNum;
	private String weight;
	private String status;
	private String origin;
	private String strain;
	private String otherSpecies;
	private String otherStrain;
	private String comments;
	private String contractNumber;
	private String building;
	
	@RestResource(exported = false)
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private User vetTech;
	
	private boolean sedationRequired;
	private String substance;
	private String dosage;
	private String temperature;
	private String pulse;
	private String respiration;
	private String notes;
	private Long studyId;
	private Long procedureId;
	private Long scheduleId;
	
	private float volume = 0.0f;
	
	@Transient
	private String groupName;

	public String getGroupName()
	{
		return groupName;
	}

	public void setGroupName(String groupName)
	{
		this.groupName = groupName;
	}

	@ManyToOne
	private AnimalGroup group;

	public AnimalGroup getGroup()
	{
		return group;
	}

	public void setGroup(AnimalGroup group)
	{
		this.group = group;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(String comments)
	{
		this.comments = comments;
	}

	public String getOldId()
	{
		return oldId;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getAnimalId()
	{
		return animalId;
	}

	public void setAnimalId(String animalId)
	{
		this.animalId = animalId;
	}

	public void setOldId(String oldId)
	{
		this.oldId = oldId;
	}

	public String getSpecies()
	{
		return species;
	}

	public void setSpecies(String species)
	{
		this.species = species;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public String getClient()
	{
		return client;
	}

	public void setClient(String client)
	{
		this.client = client;
	}

	public Date getDateOfBirth()
	{
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth)
	{
		this.dateOfBirth = dateOfBirth;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public Date getRecievedDate()
	{
		return recievedDate;
	}

	public void setRecievedDate(Date recievedDate)
	{
		this.recievedDate = recievedDate;
	}

	public Date getDepartedDate()
	{
		return departedDate;
	}

	public void setDepartedDate(Date departedDate)
	{
		this.departedDate = departedDate;
	}

	public String getRoomNum()
	{
		return roomNum;
	}

	public void setRoomNum(String roomNum)
	{
		this.roomNum = roomNum;
	}

	/*
	 * public String getVirus() { return virus; }
	 * 
	 * public void setVirus(String virus) { this.virus = virus; }
	 */

	public String getWeight()
	{
		return weight;
	}

	public void setWeight(String weight)
	{
		this.weight = weight;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getOrigin()
	{
		return origin;
	}

	public void setOrigin(String origin)
	{
		this.origin = origin;
	}

	public String getStrain()
	{
		return strain;
	}

	public void setStrain(String strain)
	{
		this.strain = strain;
	}

	public String getOtherSpecies()
	{
		return otherSpecies;
	}

	public void setOtherSpecies(String otherSpecies)
	{
		this.otherSpecies = otherSpecies;
	}

	public String getOtherStrain()
	{
		return otherStrain;
	}

	public void setOtherStrain(String otherStrain)
	{
		this.otherStrain = otherStrain;
	}

	public String getTemperature()
	{
		return temperature;
	}

	public void setTemperature(String temperature)
	{
		this.temperature = temperature;
	}

	public String getPulse()
	{
		return pulse;
	}

	public void setPulse(String pulse)
	{
		this.pulse = pulse;
	}

	public String getRespiration()
	{
		return respiration;
	}

	public void setRespiration(String respiration)
	{
		this.respiration = respiration;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}
	
	public boolean isSedationRequired()
	{
		return sedationRequired;
	}

	public void setSedationRequired(boolean sedationRequired)
	{
		this.sedationRequired = sedationRequired;
	}

	public String getSubstance()
	{
		return substance;
	}

	public void setSubstance(String substance)
	{
		this.substance = substance;
	}

	public String getDosage()
	{
		return dosage;
	}

	public void setDosage(String dosage)
	{
		this.dosage = dosage;
	}

	public Long getStudyId()
	{
		return studyId;
	}

	public void setStudyId(Long studyId)
	{
		this.studyId = studyId;
	}

	public Long getProcedureId()
	{
		return procedureId;
	}

	public void setProcedureId(Long procedureId)
	{
		this.procedureId = procedureId;
	}

	public Long getScheduleId()
	{
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId)
	{
		this.scheduleId = scheduleId;
	}

	public String getContractNumber()
	{
		return contractNumber;
	}

	public void setContractNumber(String contractNumber)
	{
		this.contractNumber = contractNumber;
	}

	public User getVetTech()
	{
		return vetTech;
	}

	public void setVetTech(User vetTech)
	{
		this.vetTech = vetTech;
	}

	public float getVolume()
	{
		return volume;
	}

	public void setVolume(float volume)
	{
		this.volume = volume;
	}

	public String getBuilding()
	{
		return building;
	}

	public void setBuilding(String building)
	{
		this.building = building;
	}

	// public List<AnimalTestResults> getAnimalTestResults() {
	// return animalTestResults;
	// }
	//
	// public void setAnimalTestResults(List<AnimalTestResults> animalTestResults) {
	// this.animalTestResults = animalTestResults;
	// }

}
