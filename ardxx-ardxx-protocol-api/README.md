# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Quick summary
----------------------
   The ardxx-api files are used for the backend services.  The project is setup using springboot.
************************
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
***************************
 How do I get set up?
-----------------------------
 

 Download mysql database and install

 Create a schema called ardxx

* *git clone https://username@bitbucket.org/ardxx/tn-commons.git*

* *cd tn-commons*

* *mvn clean install*
 
* *git clone https://username@bitbucket.org/ardxx/tn-security.git*

* *cd tn-security*

* *mvn clean install*

* *git clone https://username@bitbucket.org/ardxx/ardxx-protocol-api.git*

* *cd ardxx-protocol-api*

* *mvn clean install*

 Run the server side application 

* *java -jar -Dspring.profiles.active=dev ./ardxx-api-0.0.1-SNAPSHOT.jar*  for starting the app in dev profile

* *http://localhost:8080/health*

 Enter username/password as* user / password*


***********

Configuration
-----------------

Application config is in *application.yaml*
There are currently 4 profiles maintained which are dev, uat, qa and www
Depending on the profile the application can be started as

* *java -jar -Dspring.profiles.active=www ./ardxx-api-0.0.1-SNAPSHOT.jar* for www profile when one installs this api as http://www.ardxx.com


******************
Database configuration
-------------------------------
* Database config is in *application.yaml*
* Seed data will be loaded according to profile used when starting up the api application.