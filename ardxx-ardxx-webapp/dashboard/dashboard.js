angular.module('dashboard', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('dashboard').config(function($stateProvider) {

    $stateProvider.state('dashboard', {
        url: '/dashboard',
        templateUrl: 'dashboard/partial/dashboard/dashboard.html',
        data: { pageTitle: 'Home' }
    });
    /* Add New States Above */

});

