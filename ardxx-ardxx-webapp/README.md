# ARDXX - Web Application#

Web application for ARDXX.

>This repository has the web app for ARDXX, built using AngularJs

### Setup instructions for Dev ###

Download and install Nodejs from below link

```sh
https://nodejs.org
```

Clone app code into local system

```sh
git clone https://bitbucket.org/ardxx/ardxx-webapp.git
```

Install grunt, bower:

```sh
npm install -g grunt bower
```

Install required bower and npm components:

```sh
npm install
bower install
```

###Running application###

To run the app with grunt and nodejs, open command prompt navigate to webapp folder and execute below command
```sh
grunt serve
```

###Bundling app for distributable package###

```sh
grunt build:staging
```

>Files will be generated in "dist" folder with all the required css and js files minified.
>All the html files except index.html will be converted to js code (using ngtemplates), so no html files will be included in dist folder apart from index.html

###Running app without nodejs###

After building app all the files in dist folder can be deployed on any of the HTTP server and that gives a fully functional app working on the server