angular.module('ardxx').directive('floatingInput', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attrs, fn) {
		    if (element.val() !== '') {
		        element.addClass('static').addClass('dirty');
		    }
		    scope.$watch(attrs.ngModel, function (value) {
		        if (value && value !== '') {
		            element.addClass('static').addClass('dirty');
		        }
		    });
			element.on('keyup change', function(e) {
				if ($.trim(element.val()) !== '') {
					element.addClass('dirty').removeClass('static');
				} else {
					element.removeClass('dirty').removeClass('static');
				}
			});
		}
	};
});