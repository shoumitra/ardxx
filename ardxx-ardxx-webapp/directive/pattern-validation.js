angular.module('ardxx').directive('patternValidation', function() {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, element, attrs, ctrl) {
			//((?=.*\d)(?=.*[A-Z])(?=.*\W).{8,8}) - 8 Char, one Caps, one special, one numeric
			ctrl.$parsers.unshift(function(viewValue) {
				var patt = new RegExp(attrs.patternValidation);
				var isValid = patt.test(viewValue);
	            ctrl.$setValidity('validPattern', isValid);
	            return viewValue;
			});
		}
	};
});