angular.module('ardxx').directive('validPassword', function() {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, element, attrs, ctrl) {
			//((?=.*\d)(?=.*[A-Z])(?=.*\W).{8,8}) - 8 Char, one Caps, one special, one numeric
			ctrl.$parsers.unshift(function(viewValue) {
				var patt = new RegExp(attrs.patternValidator);
				var isValid = patt.test(viewValue);
	            ctrl.$setValidity('validPassword', isValid);
	            return viewValue;
			});
		}
	};
})
.filter('nospace', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
    };
});
