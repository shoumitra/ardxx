angular.module('study').controller('StudyListCtrl', function ($scope, $rootScope, DTOptionsBuilder, studyService, toaster) {

    var roles = {
        chairmanRole: 'CHR_PER',
        piRole: 'PRI_INV',
        memberRole: 'COM_MEM',
        labRole: 'LAB_VET',
        researchRole: 'RES_SCI',
        vetTech: 'VET_TEC',
        prjManager: 'PJT_MGR'
    };

    $scope.isChairman = $rootScope.currentUser.role.indexOf(roles.chairmanRole) > -1;
    $scope.isCM = $rootScope.currentUser.role.indexOf(roles.memberRole) > -1;
    $scope.isPI = $rootScope.currentUser.role.indexOf(roles.piRole) > -1;
    $scope.isLab = $rootScope.currentUser.role.indexOf(roles.labRole) > -1;
    $scope.isRes = $rootScope.currentUser.role.indexOf(roles.researchRole) > -1;
    $scope.isVetTech = $rootScope.currentUser.role.indexOf(roles.vetTech) > -1;
    $scope.isPM = $rootScope.currentUser.role.indexOf(roles.prjManager) > -1;

    var languageOptions = {
        "lengthMenu": '_MENU_ entries per page',
        "search": '<i class="fa fa-search"></i>',
        "paginate": {
            "previous": '<i class="fa fa-angle-left"></i>',
            "next": '<i class="fa fa-angle-right"></i>'
        }
    };
    $scope.canCreate = true;
    $scope.dtOptions = DTOptionsBuilder.newOptions()
		.withDOM('lCfrtip')
		.withOption('language', languageOptions)
		.withColVis()
		.withColVisOption('aiExclude', []);

    window.scope = $scope;

    $scope.studyList = [];

    studyService.studyList()
        .then(function (studyList) {
            if (studyList.status && studyList.status === 404) {

            } else {
                $scope.studyList = studyList.content;
            }
        },
            function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
});
