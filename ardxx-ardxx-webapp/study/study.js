angular.module('study', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('study').config(function($stateProvider) {

    $stateProvider.state('study-list', {
        url: '/study-list',
        templateUrl: 'study/partial/study-list/study-list.html',
        data: { pageTitle: 'Study List' }
    });
    $stateProvider.state('studyDetails', {
        url: '/study/:id',
        templateUrl: 'study/partial/studyDetails/studyDetails.html',
        data: { pageTitle: 'Study Details' }
    });
    $stateProvider.state('scheduledetails', {
        url: '/scheduledetails/:id/:time',
        templateUrl: 'study/partial/procedureDetails/procedureDetails.html',
        data: { pageTitle: 'Schedule Details' }
    });
    /* Add New States Above */

});

