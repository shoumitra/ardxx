angular.module('animals', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate']);

angular.module('animals').config(function ($stateProvider) {

    $stateProvider.state('animal-list', {
        url: '/animals',
        templateUrl: 'animals/partial/animal-list/animal-list.html',
        data: { pageTitle: 'Animals' }
    })
    .state('animalResults', {
        url: '/animalResults/:id',
        templateUrl: 'animals/partial/animalResults/animalResults.html',
        data: { pageTitle: 'Animal test results' }
    })
    .state('animalDetails', {
        url: '/animalDetails/:id',
        templateUrl: 'animals/partial/animalDetails/animalDetails.html',
        data: { pageTitle: 'Animal Details' }
    });
});

