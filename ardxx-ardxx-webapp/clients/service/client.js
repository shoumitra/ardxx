/**
 * 
 */

angular.module('clients').factory('clientService',function($http,configService,$q) {
	
	var clients = {};
	
	clients.clientsList = function() {
		var deferred = $q.defer();
        $http.get(configService.getHostName + 'clients')
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
	};
	
	clients.create = function(client) {
		var deferred = $q.defer();
		$http.post(configService.getHostName + 'clients', client)
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
	};
	
	clients.getClientDetails = function (clientId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'clients/' + clientId)
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
    };
	
	return clients;
});