/**
 * @author user
 */

angular.module('clients', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('clients').config(function($stateProvider) {
	
	$stateProvider.state('clients-list', {
        url: '/clients-list',
        templateUrl: 'clients/partial/clients-list/clients-list.html',
        data: { pageTitle: 'Clients' }
    })
    .state('clientProfile', {
        url: '/clientprofile/:id',
        templateUrl: 'clients/partial/clientProfile/clientProfile.html',
        data: { pageTitle: 'Client Profile' }
    });
	
});