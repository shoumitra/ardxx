/**
 * 
 */
angular.module('clients').controller('ClientProfileCtrl', function ($scope, $rootScope, $state, $stateParams, toaster, clientService) {
	$scope.clientId = parseInt($stateParams.id) || 0;
	
	if ($scope.clientId && $scope.clientId !== 0) {
		$scope.detailsEditable = false;
		clientService.getClientDetails($scope.clientId)
			.then(function (data) {
				$scope.client = data;
			}, function (data) {
                toaster.pop('error', 'Error', data.errorMessage);
            });
	} else {
		$scope.client = {};
		$scope.detailsEditable = true;
		$scope.isSave = false;
	}
	
	$scope.saveProfile = function(isValid) {
		$scope.isSave = true;
		if (isValid) {
			if ($scope.clientId && $scope.clientId !== 0) {
				toaster.pop('success', 'Updated', 'User updated successfully.');
			} else {
				clientService.create(this.client);
				toaster.pop('success', 'Updated', 'User created successfully.');
			}
		} else {
			$scope.isSave = false;
            toaster.pop('error', 'Error', 'Please correct details.');
        }
	};
});