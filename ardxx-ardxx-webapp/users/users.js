angular.module('users', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('users').config(function($stateProvider) {

    $stateProvider.state('users-list', {
        url: '/users-list',
        templateUrl: 'users/partial/users-list/users-list.html',
        data: { pageTitle: 'Users' }
    })
   .state('userProfile', {
        url: '/userprofile/:id',
        templateUrl: 'users/partial/userProfile/userProfile.html',
        data: { pageTitle: 'User Profile' }
    })
    .state('myProfile', {
        url: '/myProfile',
        templateUrl: 'users/partial/userProfile/userProfile.html',
        data: { pageTitle: 'My Profile' }
    });
    /* Add New States Above */

});

