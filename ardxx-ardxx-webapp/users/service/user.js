angular.module('users').factory('userService',function($http,configService,$q) {

    var users = {};

    users.usersList = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'users')
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
    };

    users.rolesList = function () {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'roles')
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
    };

    users.create = function (user) {
        var deferred = $q.defer();
        $http.post(configService.getHostName + 'users', user)
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
    };

    users.getUserDetails = function (userId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'users/' + userId)
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
    };

    users.updateDetails = function (user) {
        var deferred = $q.defer();
        $http.put(configService.getHostName + 'users', user)
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
    };


	return users;
});