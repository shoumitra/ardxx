angular.module('clinicalProcedures', ['ui.bootstrap','ui.utils','ui.router','ngAnimate']);

angular.module('clinicalProcedures').config(function($stateProvider) {

    $stateProvider.state('procedure-list', {
        url: '/procedurelist',
        templateUrl: 'clinicalProcedures/partial/procedure-list/procedure-list.html',
        data: { pageTitle: 'Procedure List' }
    });
    $stateProvider.state('procedure-create', {
        url: '/procedure-create',
        templateUrl: 'clinicalProcedures/partial/procedure-create/procedure-create.html',
        data: { pageTitle: 'Create Procedure' }
    });
    $stateProvider.state('procedure-details', {
        url: '/procedure-details/:id',
        templateUrl: 'clinicalProcedures/partial/procedure-details/procedure-details.html',
        data: { pageTitle: 'Procedure Details' }
    });
    /* Add New States Above */

});

