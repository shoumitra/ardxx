angular.module('protocol').factory('pdfService', function($filter) {

	var pdfService = {};
	var positionBottom = 0;
	var currenttPosition = 0;
	var pages = 1;
	var footerText = 'BIOQUAL IACUC Form, 04/14';

	var margins = {
		top: 55,
		bottom: 10,
		left: 21,
		width: 25
	};
	pdfService.printPdf = function(protocolDetails, docName) {
		positionBottom = 0;
		currenttPosition = 0;
		pages = 1;

		var doc = new window.jsPDF('p', 'pt', 'letter');
		doc.setFont("times");
		doc.setFontStyle('bold');
		doc.setFontSize(8);
		doc.text(margins.left + 285, 770, '' + pages);
		doc.setFontStyle('normal');
		doc.text(margins.left + 430, 780, footerText);
		//Title box
		doc.rect(margins.left, margins.top, 400.55, 117.8);
		//Set title
		doc.setFontSize(12);
		doc.setFontType("bold");
		doc.text(90.1, 110, 'BIOQUAL ANIMAL STUDY PROPOSAL FORM');


		doc.line(351, 112, 90.1, 112); // Underline for title

		//Title right block
		doc.rect(421.55, 55, 157.5, 117.8);

		//Leave Blank cell
		doc.setDrawColor(0);
		doc.setFillColor(242, 242, 242);
		doc.rect(421.55, 55, 157.5, 13.9, 'FD');
		doc.setFontSize(8);
		doc.text(478, 64, 'Leave Blank');

		//Proposal Number
		doc.setFontSize(11);
		doc.setFontType("normal");
		doc.setLineWidth(0);

		doc.text(451, 80, 'Proposal #:');
		if (protocolDetails.proposalNumber !== null) {
			doc.text(505, 80, protocolDetails.proposalNumber);
		}
		doc.line(579, 85, 505, 85);

		//Approval Date
		doc.text(432, 98, 'Approval Date:');
		if (protocolDetails.approvalDate) {
			doc.text(505, 98, moment(protocolDetails.approvalDate).format('MM/DD/YYYY'));
		}
		doc.line(579, 102, 505, 102);

		//Expiration Date
		doc.text(427, 115.4, 'Expiration Date:');
		if (protocolDetails.expirationDate) {
			doc.text(505, 116, moment(protocolDetails.expirationDate).format('MM/DD/YYYY'));
		}
		doc.line(421.55, 119.4, 579.45, 119.4);

		//Annual Review Due
		doc.setFontSize(10);
		doc.setFontType("bold");
		doc.text(427, 129.4, 'Annual Review Due');
		doc.line(421.55, 136.4, 579.45, 136.4);

		doc.setFontSize(11);
		doc.setFontType("normal");

		doc.text(461, 152.4, '1Year:');
		doc.setFontSize(7);
		doc.text(465, 148, 'st');
		doc.setFontSize(11);
		if (protocolDetails.annualReviewDueFirstYear) {
			doc.text(505, 149.4, moment(protocolDetails.annualReviewDueFirstYear).format('MM/DD/YYYY'));
		}
		doc.line(579, 152.4, 505, 152.4);

		doc.setFontSize(7);
		doc.text(463, 164, 'nd');
		doc.setFontSize(11);
		doc.text(458, 168.4, '2Year:');
		if (protocolDetails.annualReviewDueSecondYear) {
			doc.text(505, 168.4, moment(protocolDetails.annualReviewDueSecondYear).format('MM/DD/YYYY'));
		}

		doc.rect(margins.left, margins.top + 117.8, 558, 200);
		positionBottom = margins.top + 117.8 + 17.3;

		doc.line(margins.left, positionBottom, 579, positionBottom);

		positionBottom = positionBottom + 16.1;

		var admin = protocolDetails.protocolAdministrativeData;
		doc.setFontSize(10);
		doc.setFontType("bold");
		doc.text(margins.left + 7, positionBottom - 5, 'A.ADMINISTRATIVE DATA:');
		doc.line(margins.left, positionBottom, 579, positionBottom);

		doc.setFontType("normal");
		positionBottom = positionBottom + 16.1;
		doc.text(margins.left + 6, positionBottom - 5, 'Principal Investigator:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		if (admin.principalInvestigator !== null) {
			doc.text(margins.left + 240, positionBottom - 5, admin.principalInvestigator.firstName + ' ' + admin.principalInvestigator.lastName);
		}
		doc.line(margins.left + 234, positionBottom - 16.1, margins.left + 234, positionBottom);

		positionBottom = positionBottom + 16.1;
		doc.text(margins.left + 6, positionBottom - 5, 'Building/Room:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		if (admin.building !== null) {
			doc.text(margins.left + 240, positionBottom - 5, admin.building);
		}
		doc.line(margins.left + 234, positionBottom - 16.1, margins.left + 234, positionBottom);

		positionBottom = positionBottom + 16.1;
		doc.text(margins.left + 6, positionBottom - 5, 'Telephone:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		if (admin.phone !== null) {
			doc.text(margins.left + 240, positionBottom - 5, admin.phone);
		}
		doc.line(margins.left + 234, positionBottom - 16.1, margins.left + 234, positionBottom);

		positionBottom = positionBottom + 16.1;
		doc.text(margins.left + 6, positionBottom - 5, 'Fax:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		if (admin.fax !== null) {
			doc.text(margins.left + 240, positionBottom - 5, admin.fax);
		}
		doc.line(margins.left + 234, positionBottom - 16.1, margins.left + 234, positionBottom);

		positionBottom = positionBottom + 16.1;
		doc.text(margins.left + 6, positionBottom - 5, 'Email:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		if (admin.email !== null) {
			doc.text(margins.left + 240, positionBottom - 5, admin.email);
		}
		doc.line(margins.left + 234, positionBottom - 16.1, margins.left + 234, positionBottom);

		positionBottom = positionBottom + 16.1;
		doc.text(margins.left + 6, positionBottom - 5, 'Client:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		if (admin.client !== null) {
			doc.text(margins.left + 240, positionBottom - 5, admin.client);
		}
		doc.line(margins.left + 234, positionBottom - 16.1, margins.left + 234, positionBottom);

		positionBottom = positionBottom + 16.1;
		doc.text(margins.left + 6, positionBottom - 5, 'Project Title:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		if (admin.projectTitle !== null) {
			doc.text(margins.left + 240, positionBottom - 5, admin.projectTitle);
		}
		doc.line(margins.left + 234, positionBottom - 16.1, margins.left + 234, positionBottom);

		positionBottom = positionBottom + 5;
		doc.rect(margins.left + 6, positionBottom, 15, 15); //Initial Submission check
		if (admin.initialSubmission) {
			doc.line(margins.left + 9, positionBottom + 6, margins.left + 13, positionBottom + 10);
			doc.line(margins.left + 19, positionBottom + 3, margins.left + 13, positionBottom + 10);
		}

		doc.rect(margins.left + 180, positionBottom, 15, 15); //Renewal of Proposal Number check
		if (admin.renewalProposalNumber !== null && admin.renewalProposalNumber !== '') {
			doc.line(margins.left + 182, positionBottom + 6, margins.left + 186, positionBottom + 10);
			doc.line(margins.left + 192, positionBottom + 3, margins.left + 186, positionBottom + 10);
		}
		positionBottom = positionBottom + 12;
		doc.text(margins.left + 30, positionBottom, 'Initial Submission');

		doc.text(margins.left + 200, positionBottom, 'Renewal of Proposal Number:');
		if (admin.renewalProposalNumber !== null) {
			doc.text(margins.left + 340, positionBottom - 2, admin.renewalProposalNumber);
		}
		doc.line(margins.left + 330, positionBottom, 579, positionBottom);

		positionBottom = positionBottom + 16;
		doc.rect(margins.left + 6, positionBottom, 15, 15);
		if (admin.modification) {
			doc.line(margins.left + 9, positionBottom + 6, margins.left + 13, positionBottom + 10);
			doc.line(margins.left + 19, positionBottom + 3, margins.left + 13, positionBottom + 10);
		}
		positionBottom = positionBottom + 12;
		doc.text(margins.left + 30, positionBottom, 'Modification');

		positionBottom = positionBottom + 15;

		doc.rect(margins.left, positionBottom, 558, 370);
		positionBottom = positionBottom + 15;
		doc.setFontSize(9);
		doc.text(margins.left + 6, positionBottom, 'List the names of all individuals authorized to conduct procedures involving animals under this proposal,');
		positionBottom = positionBottom + 63.5;
		doc.line(margins.left, positionBottom, 579, positionBottom);
		doc.setFontSize(10);

		positionBottom = positionBottom + 15;
		doc.text(margins.left + 6, positionBottom, 'PI doing hands-on animal work?');

		doc.setFontType('bold');
		doc.text(margins.left + 200, positionBottom, 'Yes');
		doc.rect(margins.left + 220, positionBottom - 10, 15, 15);
		if (admin.piHandsOnAnimalWork) {
			doc.line(margins.left + 223, positionBottom - 3, margins.left + 227, positionBottom);
			doc.line(margins.left + 233, positionBottom - 7, margins.left + 227, positionBottom);
		}
		doc.text(margins.left + 250, positionBottom, 'No');
		doc.rect(margins.left + 270, positionBottom - 10, 15, 15);

		if (!admin.piHandsOnAnimalWork) {
			doc.line(margins.left + 273, positionBottom - 3, margins.left + 277, positionBottom);
			doc.line(margins.left + 283, positionBottom - 7, margins.left + 277, positionBottom);
		}

		doc.setFontType('normal');
		positionBottom = positionBottom + 20;
		doc.text(margins.left + 6, positionBottom, 'Co-investigator(s): (Maximum three investigators; Include affiliation)');
		positionBottom = positionBottom + 10;
		doc.line(margins.left + 6, positionBottom, 573, positionBottom);
		doc.line(margins.left + 6, positionBottom, margins.left + 6, positionBottom + 16.1);
		doc.line(margins.left + 199, positionBottom, margins.left + 199, positionBottom + 16.1);
		doc.line(margins.left + 392, positionBottom, margins.left + 392, positionBottom + 16.1);
		doc.line(margins.left + 551, positionBottom, margins.left + 551, positionBottom + 16.1);
		positionBottom = positionBottom + 16.1;
		doc.line(margins.left + 6, positionBottom, 573, positionBottom);

		positionBottom = positionBottom + 20;
		doc.text(margins.left + 6, positionBottom, 'Other Personnel:');

		positionBottom = positionBottom + 10;
		doc.line(margins.left + 6, positionBottom, 573, positionBottom);
		doc.line(margins.left + 6, positionBottom, margins.left + 6, positionBottom + 16.1);
		doc.line(margins.left + 199, positionBottom, margins.left + 199, positionBottom + 16.1);
		doc.line(margins.left + 392, positionBottom, margins.left + 392, positionBottom + 16.1);
		doc.line(margins.left + 551, positionBottom, margins.left + 551, positionBottom + 16.1);
		positionBottom = positionBottom + 16.1;
		doc.line(margins.left + 6, positionBottom, 573, positionBottom);

		doc.line(margins.left + 6, positionBottom, margins.left + 6, positionBottom + 16.1);
		doc.line(margins.left + 199, positionBottom, margins.left + 199, positionBottom + 16.1);
		doc.line(margins.left + 392, positionBottom, margins.left + 392, positionBottom + 16.1);
		doc.line(margins.left + 551, positionBottom, margins.left + 551, positionBottom + 16.1);
		positionBottom = positionBottom + 16.1;
		doc.line(margins.left + 6, positionBottom, 573, positionBottom);

		doc.line(margins.left + 6, positionBottom, margins.left + 6, positionBottom + 16.1);
		doc.line(margins.left + 199, positionBottom, margins.left + 199, positionBottom + 16.1);
		doc.line(margins.left + 392, positionBottom, margins.left + 392, positionBottom + 16.1);
		doc.line(margins.left + 551, positionBottom, margins.left + 551, positionBottom + 16.1);
		positionBottom = positionBottom + 16.1;
		doc.line(margins.left + 6, positionBottom, 573, positionBottom);

		doc.line(margins.left + 6, positionBottom, margins.left + 6, positionBottom + 16.1);
		doc.line(margins.left + 199, positionBottom, margins.left + 199, positionBottom + 16.1);
		doc.line(margins.left + 392, positionBottom, margins.left + 392, positionBottom + 16.1);
		doc.line(margins.left + 551, positionBottom, margins.left + 551, positionBottom + 16.1);
		positionBottom = positionBottom + 16.1;
		doc.line(margins.left + 6, positionBottom, 573, positionBottom);

		positionBottom = positionBottom + 20;
		doc.text(margins.left + 6, positionBottom, 'Comments:');

		positionBottom = positionBottom + 10;
		doc.rect(margins.left + 6, positionBottom, 545, 100);
		if (admin.comments !== null) {
			doc.text(margins.left + 12, positionBottom + 10, admin.comments);
		}

		doc.addPage();
		pages++;
		positionBottom = margins.top;
		doc.setFontStyle('bold');
		doc.setFontSize(8);
		doc.text(margins.left + 285, 770, '' + pages);
		doc.setFontStyle('normal');
		doc.text(margins.left + 430, 780, footerText);

		doc.rect(margins.left, margins.top, 558, 16.1);

		positionBottom = positionBottom + 16.1;

		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom - 5, 'B.ANIMAL REQUIREMENTS:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		doc.setFontStyle('normal');

		$.each(protocolDetails.animalRequirements, function() {
			if (positionBottom + 182 >= 791) {
				doc.addPage();

				pages++;
				positionBottom = margins.top;
				doc.setFontStyle('bold');
				doc.setFontSize(8);
				doc.text(margins.left + 285, 770, '' + pages);
				doc.setFontStyle('normal');
				doc.text(margins.left + 430, 780, footerText);
				doc.setFontSize(10);
			} else {
				positionBottom = positionBottom + 5;
			}

			doc.rect(margins.left, positionBottom, 558, 162);
			var req = this;
			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Species:');
			if (req.species) {
				doc.text(margins.left + 166, positionBottom - 5, req.species);
			}
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Stock/Strain:');
			if (req.strain) {
				doc.text(margins.left + 166, positionBottom - 5, req.strain);
			}
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Age/Weight/Size:');
			if (req.age) {
				doc.text(margins.left + 166, positionBottom - 5, req.age);
			}
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Sex:');
			if (req.sex) {
				doc.text(margins.left + 166, positionBottom - 5, req.sex);
			}
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Source(s):');
			if (req.sources) {
				doc.text(margins.left + 166, positionBottom - 5, req.sources);
			}
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Animal Holding Location(s):');
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Animal Procedure Location(s):');
			if (req.procedureLocations) {
				doc.text(margins.left + 166, positionBottom - 5, req.procedureLocations);
			}
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Bacteriologic/Viral Status:');
			if (req.bacteriologyOrViral) {
				doc.text(margins.left + 166, positionBottom - 5, req.bacteriologyOrViral);
			}
			doc.line(margins.left, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom);

			positionBottom = positionBottom + 16.2;
			doc.text(margins.left + 6, positionBottom - 5, 'Number of Animals:');

			doc.line(margins.left + 160, positionBottom, 579, positionBottom);
			doc.line(margins.left + 160, positionBottom - 16.2, margins.left + 160, positionBottom + 16.2);

			doc.text(margins.left + 190, positionBottom - 5, 'Species');
			if (req.numberOfAnimals && req.numberOfAnimals.species) {
				doc.text(margins.left + 166, positionBottom + 11, req.numberOfAnimals.species);
			}
			doc.line(margins.left + 245.6, positionBottom - 16.2, margins.left + 245.6, positionBottom + 16.2);

			doc.text(margins.left + 270, positionBottom - 5, 'Year 1');
			if (req.numberOfAnimals && req.numberOfAnimals.year1) {
				doc.text(margins.left + 270, positionBottom + 11, '' + req.numberOfAnimals.year1);
			}
			doc.line(margins.left + 322.1, positionBottom - 16.2, margins.left + 322.1, positionBottom + 16.2);

			doc.text(margins.left + 350, positionBottom - 5, 'Year 2');
			if (req.numberOfAnimals && req.numberOfAnimals.year2) {
				doc.text(margins.left + 350, positionBottom + 11, '' + req.numberOfAnimals.year2);
			}
			doc.line(margins.left + 398, positionBottom - 16.2, margins.left + 398, positionBottom + 16.2);

			doc.text(margins.left + 426, positionBottom - 5, 'Year 3');
			if (req.numberOfAnimals && req.numberOfAnimals.year3) {
				doc.text(margins.left + 426, positionBottom + 11, '' + req.numberOfAnimals.year3);
			}
			doc.line(margins.left + 475, positionBottom - 16.2, margins.left + 475, positionBottom + 16.2);

			doc.text(margins.left + 500, positionBottom - 5, 'Total');
			if (req.numberOfAnimals && req.numberOfAnimals.total) {
				doc.text(margins.left + 500, positionBottom + 11, '' + req.numberOfAnimals.total);
			}
			positionBottom = positionBottom + 20;
		});

		if (positionBottom + 89 >= 791) {
			doc.addPage();

			pages++;
			positionBottom = margins.top;
			doc.setFontStyle('bold');
			doc.setFontSize(8);
			doc.text(margins.left + 285, 770, '' + pages);
			doc.setFontStyle('normal');
			doc.text(margins.left + 430, 780, footerText);
			doc.setFontSize(10);
		} else {
			positionBottom = positionBottom + 15;
		}

		doc.rect(margins.left, positionBottom, 558, 74);

		positionBottom = positionBottom + 12;

		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom - 3, 'C.TRANSPORTATION:');
		doc.line(margins.left, positionBottom, 579, positionBottom);
		doc.setFontStyle('normal');
		doc.setFontSize(9);
		doc.text(margins.left + 126, positionBottom - 3, 'If animals will be transported between facilities, describe the methods and containment to be utilized.');

		if (positionBottom + 380 >= 791) {
			doc.addPage();

			pages++;
			positionBottom = margins.top;
			doc.setFontStyle('bold');
			doc.setFontSize(8);
			doc.text(margins.left + 285, 770, '' + pages);
			doc.setFontStyle('normal');
			doc.text(margins.left + 430, 780, footerText);
			doc.setFontSize(10);
		} else {
			positionBottom = positionBottom + 74;
		}

		doc.rect(margins.left, positionBottom, 558, 360);

		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 10, 'D.STUDY OBJECTIVES:');
		doc.setFontStyle('normal');
		doc.setFontSize(9);

		positionBottom = positionBottom + 20;

		var lines = doc.splitTextToSize('1.Provide no more than a 300 word summary of the objectives of this work. Why is this work important/interesting? How might this work benefit humans and/or animals? This should be written so that a non-scientist can easily understand it. Please eliminate or minimize abbreviations, technical terms, and jargon. Where necessary, they should be defined.',
			550);
		doc.text(margins.left + 6, positionBottom, lines);

		positionBottom += (lines.length) * 9;
		lines = doc.splitTextToSize('2. For renewal proposals, provide a brief summary of recent progress made using the last three-years’ animals.',
			550);
		doc.text(margins.left + 6, positionBottom, lines);
		positionBottom += (lines.length) * 9;
		lines = doc.splitTextToSize('3. For new and renewal proposals, a brief synopsis of the scientific rationale or foundation for the study may optionally be provided.',
			550);
		doc.text(margins.left + 6, positionBottom, lines);
		positionBottom += (lines.length) * 9;
		lines = doc.splitTextToSize('4. For modifications, provide a summary of why the modification is required.',
			550);
		doc.text(margins.left + 6, positionBottom, lines);
		positionBottom += (lines.length) * 9;
		doc.line(margins.left, positionBottom, 579, positionBottom);
		doc.setFontSize(10);
		positionBottom = positionBottom + 20;

		var questionDetails = null;


		for (var qcount = 1; qcount <= 4; qcount++) {
			doc.text(margins.left + 25, positionBottom, qcount + '.');

			questionDetails = null;
			questionDetails = $filter('filter')(protocolDetails.studyObjective.userInputs, {
				label: qcount
			});
			lines = '';
			if (questionDetails && questionDetails.length > 0) {
				lines = doc.splitTextToSize(questionDetails[0].value, 510);
				doc.text(margins.left + 40, positionBottom, lines);
			}

			positionBottom += 70;
		}

		if (positionBottom + 320 >= 791) {
			doc.addPage();

			pages++;
			positionBottom = margins.top;
			doc.setFontStyle('bold');
			doc.setFontSize(8);
			doc.text(margins.left + 285, 770, '' + pages);
			doc.setFontStyle('normal');
			doc.text(margins.left + 430, 780, footerText);
			doc.setFontSize(10);
		} else {
			positionBottom = positionBottom + 74;
		}

		doc.rect(margins.left, positionBottom, 558, 320);

		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 10, 'E. RATIONALE FOR USE OF ANIMALS:');
		doc.setFontStyle('normal');
		doc.setFontSize(9);

		positionBottom = positionBottom + 20;

		lines = doc.splitTextToSize('1) Explain your rationale for animal use. 2) Justify the appropriateness of the species selected. 3)Justify the number of animals to be used.',
			550);
		doc.text(margins.left + 6, positionBottom, lines);

		positionBottom += (lines.length) * 9;
		doc.line(margins.left, positionBottom, 579, positionBottom);
		doc.setFontSize(10);
		positionBottom = positionBottom + 20;

		for (var count = 1; count <= 3; count++) {
			doc.text(margins.left + 25, positionBottom, count + '.');

			questionDetails = null;
			questionDetails = $filter('filter')(protocolDetails.rationalUseOfAnimals.userInputs, {
				label: count
			});
			lines = '';
			if (questionDetails && questionDetails.length > 0) {
				lines = doc.splitTextToSize(questionDetails[0].value, 510);
				doc.text(margins.left + 40, positionBottom, lines);
			}

			positionBottom += 90;
		}
		positionBottom = positionBottom + 20;
		doc.rect(margins.left, positionBottom, 558, 40);

		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 10, 'F. DESCRIPTION OF EXPERIMENTAL DESIGN AND ANIMAL PROCEDURES:');
		doc.setFontStyle('normal');
		doc.setFontSize(9);

		positionBottom = positionBottom + 20;

		lines = doc.splitTextToSize('Briefly explain the experimental design and specify all animal procedures performed. This description should allow the ACUC to understand the experimental course of an animal from its entry into the experiment to the endpoint of the study. Specifically address the following:', 550);
		currenttPosition = positionBottom + 20;
		doc.text(margins.left + 6, positionBottom, lines);
		positionBottom = positionBottom + 40;

		questionDetails = null;

		doc.setFontSize(9);
		angular.forEach(protocolDetails.experimentalDesignQuestions, function(question) {
			if (positionBottom + 60 >= 791) {
				doc.rect(margins.left, currenttPosition, 558, positionBottom - currenttPosition);

				doc.addPage();

				pages++;
				positionBottom = margins.top;
				currenttPosition = margins.top - 20;
				doc.setFontStyle('bold');
				doc.setFontSize(8);
				doc.text(margins.left + 285, 770, '' + pages);
				doc.setFontStyle('normal');
				doc.text(margins.left + 430, 780, footerText);
				doc.setFontSize(9);
			}

			doc.setFontStyle('bold');

			doc.text(margins.left + 6, positionBottom, question.id + '.  ' + question.value);

			doc.setFontStyle('normal');
			doc.text(margins.left + (question.value.length * 4.7), positionBottom, question.hint);
			positionBottom += 13;
			lines = doc.splitTextToSize(question.text ? question.text : '', 550);
			doc.text(margins.left + 20, positionBottom, lines);
			positionBottom += ((lines.length) * 9) + 10;
		});

		doc.rect(margins.left, currenttPosition, 558, positionBottom - currenttPosition);

		positionBottom += 20;

		doc.rect(margins.left, positionBottom, 558, 40);

		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 10, 'G. MAJOR SURVIVAL SURGERY ');
		doc.setFontStyle('normal');
		doc.setFontSize(9);
		doc.text(margins.left + 170, positionBottom + 10, ' - If proposed, complete the following.');

		positionBottom += 20;
		doc.text(margins.left + 6, positionBottom + 10, 'NONE __ (check if none)');

		positionBottom += 20;

		currenttPosition = positionBottom;
		positionBottom += 20;

		angular.forEach(protocolDetails.surgeryQuestions, function(question) {
			if (positionBottom + 60 >= 791) {
				doc.rect(margins.left, currenttPosition, 558, positionBottom - currenttPosition);
				doc.addPage();

				pages++;
				positionBottom = margins.top;
				currenttPosition = margins.top - 20;
				doc.setFontStyle('bold');
				doc.setFontSize(8);
				doc.text(margins.left + 285, 770, '' + pages);
				doc.setFontStyle('normal');
				doc.text(margins.left + 430, 780, footerText);
				doc.setFontSize(9);
			}

			doc.setFontStyle('bold');

			doc.text(margins.left + 6, positionBottom, question.id + '.  ' + question.value);
			doc.setFontStyle('normal');
			positionBottom += 13;
			lines = doc.splitTextToSize(question.text ? question.text : '', 550);
			doc.text(margins.left + 20, positionBottom, lines);
			positionBottom += ((lines.length) * 9) + 10;
		});
		doc.rect(margins.left, currenttPosition, 558, positionBottom - currenttPosition);

		positionBottom += 20;

		doc.rect(margins.left, positionBottom, 558, 50);

		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 10, 'H. RECORDING PAIN OR DISTRESS CATEGORY');
		doc.setFontStyle('normal');
		doc.setFontSize(9);
		doc.text(margins.left + 240, positionBottom + 10, ' - Proper use of animals, including the avoidance or minimization of discomfort,');
		positionBottom += 20;
		lines = doc.splitTextToSize('distress, and pain when consistent with sound scientific practices, is imperative. Unless the contrary is established, investigators should consider that procedures that cause pain or distress in human beings may cause pain or distress in animals.', 550);
		doc.text(margins.left + 16, positionBottom, lines);
		positionBottom += 20;
		doc.text(margins.left + 16, positionBottom, 'Check the appropriate category(ies) and indicate the approximate number of animals in each. Sum(s) should equal total from Section B.');
		positionBottom += 10;
		checkPageLength(doc, 50);
		doc.rect(margins.left, positionBottom, 558, 50);
		positionBottom += 12;
		lines = doc.splitTextToSize('IF ANIMALS ARE INDICATED IN COLUMN E, A SCIENTIFIC JUSTIFICATION IS REQUIRED TO EXPLAIN WHY THE USE OF ANESTHETICS, ANALGESICS, SEDATIVES OR TRANQUILIZERS DURING AND/OR FOLLOWING PAINFUL OR DISTRESSFUL PROCEDURES IS CONTRAINDICATED. FOR USDA REGULATED SPECIES, PLEASE COMPLETE THE EXPLANATION FOR COLUMN E LISTINGS FORM AT THE END OF THIS DOCUMENT.', 550);
		doc.text(margins.left + 16, positionBottom, lines);
		positionBottom += 38;

		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);
		doc.line(350, positionBottom, 350, positionBottom + 25);
		positionBottom += 15;
		doc.setFontStyle('bold');

		doc.text(margins.left + 380, positionBottom, 'Number of animals to be used each year');

		doc.setFontStyle('normal');
		positionBottom += 10;
		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);
		doc.line(350, positionBottom, 350, positionBottom + 25);
		doc.text(370, positionBottom + 15, 'YEAR 1');
		doc.line(425, positionBottom, 425, positionBottom + 25);
		doc.text(445, positionBottom + 15, 'YEAR 2');
		doc.line(500, positionBottom, 500, positionBottom + 25);
		doc.text(520, positionBottom + 15, 'YEAR 3');

		positionBottom += 25;

		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 15, 'USDA Column B');
		doc.setFontStyle('normal');
		doc.text(margins.left + 80, positionBottom + 15, '– No Pain or Distress');
		doc.line(350, positionBottom, 350, positionBottom + 25);
		doc.text(360, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnB.year1);
		doc.line(425, positionBottom, 425, positionBottom + 25);
		doc.text(435, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnB.year2);
		doc.line(500, positionBottom, 500, positionBottom + 25);
		doc.text(510, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnB.year3);

		positionBottom += 25;

		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 15, 'USDA Column C');
		doc.setFontStyle('normal');
		doc.text(margins.left + 80, positionBottom + 15, '– Slight (Minimal), Momentary (Transient), or No Pain or Distress');
		doc.line(350, positionBottom, 350, positionBottom + 25);
		doc.text(360, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnC.year1);
		doc.line(425, positionBottom, 425, positionBottom + 25);
		doc.text(435, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnC.year2);
		doc.line(500, positionBottom, 500, positionBottom + 25);
		doc.text(510, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnC.year3);

		positionBottom += 25;

		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 15, 'USDA Column D');
		doc.setFontStyle('normal');
		doc.text(margins.left + 80, positionBottom + 15, '– Pain or Distress Relieved By Appropriate Measures');
		doc.line(350, positionBottom, 350, positionBottom + 25);
		doc.text(360, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnD.year1);
		doc.line(425, positionBottom, 425, positionBottom + 25);
		doc.text(435, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnD.year2);
		doc.line(500, positionBottom, 500, positionBottom + 25);
		doc.text(510, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnD.year3);

		positionBottom += 25;

		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 15, 'USDA Column E');
		doc.setFontStyle('normal');
		doc.text(margins.left + 80, positionBottom + 15, '– Unrelieved Pain or Distress');
		doc.line(350, positionBottom, 350, positionBottom + 25);
		doc.text(360, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnE.year1);
		doc.line(425, positionBottom, 425, positionBottom + 25);
		doc.text(435, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnE.year2);
		doc.line(500, positionBottom, 500, positionBottom + 25);
		doc.text(510, positionBottom + 15, '' + protocolDetails.recordPainDistressCategory.usdacolumnE.year3);

		positionBottom += 25;

		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 15, 'Totals:');
		doc.setFontStyle('normal');
		doc.line(350, positionBottom, 350, positionBottom + 25);
		doc.text(360, positionBottom + 15, '');
		doc.line(425, positionBottom, 425, positionBottom + 25);
		doc.text(435, positionBottom + 15, '');
		doc.line(500, positionBottom, 500, positionBottom + 25);
		doc.text(510, positionBottom + 15, '');

		positionBottom += 25;

		checkPageLength(doc, 140);
		doc.rect(margins.left, positionBottom, 558, 140);
		positionBottom += 12;
		lines = doc.splitTextToSize('Describe in a brief narrative your consideration of alternatives to all procedures listed for Column D and E that may cause more than momentary or slight pain or distress to the animals, and your determination that alternatives that allow the attainment of the goals of the research were not available. Consideration must be given to alternative procedures, refinement of current procedures, and reduction in the number of animals used. Delineate below the methods and sources used in the search for and consideration of alternative procedures and method refinements.', 550);
		doc.text(margins.left + 6, positionBottom, lines);

		doc.setFontStyle('bold');
		positionBottom += 37;
		lines = doc.splitTextToSize('Database references must include databases (2 or more) searched, the date of the search, period covered, and keywords used. Expert consultations must include the consultant’s name and qualifications and the date and content of consult.', 550);
		doc.text(margins.left + 6, positionBottom, lines);
		doc.setFontStyle('normal');

		positionBottom += 22;
		lines = doc.splitTextToSize('It is the responsibility of the Principal Investigator to justify why alternative procedures and refinements are contraindicated.', 550);
		doc.text(margins.left + 6, positionBottom, lines);

		positionBottom += 12;
		lines = doc.splitTextToSize(protocolDetails.recordPainDistressCategory.narativeOfProcedures ? protocolDetails.recordPainDistressCategory.narativeOfProcedures : '', 550);
		doc.text(margins.left + 6, positionBottom, lines);

		positionBottom += 57;
		checkPageLength(doc, 30);
		doc.rect(margins.left, positionBottom, 558, 30);
		positionBottom += 12;
		doc.setFontStyle('bold');
		lines = doc.splitTextToSize('Note: Principal investigators must certify in Section N, Certification 5, that no valid alternative was identified to any described procedures which may cause more than momentary pain or distress, whether it is relieved or not.', 550);
		doc.text(margins.left + 6, positionBottom, lines);

		positionBottom += 25;
		checkPageLength(doc, 110);

		doc.rect(margins.left, positionBottom, 558, 110);
		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 10, 'I. ANESTHESIA, ANALGESIA, TRANQUILIZATION');
		doc.setFontStyle('normal');
		doc.setFontSize(9);
		doc.text(margins.left + 250, positionBottom + 10, ' – For animals indicated in Section H, Column D, and for animals anesthetized to');
		lines = doc.splitTextToSize('prevent distress, specify the anesthetics, analgesics, sedatives or tranquilizers that are to be used. Include the name of the agent(s), the dosage, route, and frequency of administration.', 540);
		doc.text(margins.left + 16, positionBottom + 20, lines);

		positionBottom += 20;
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 20, 'NONE __ (check if none)');

		if(protocolDetails.anesthesiaMethod.none){
			doc.line(margins.left + 34, positionBottom+17, margins.left + 38, positionBottom + 19);
			doc.line(margins.left + 38, positionBottom+19, margins.left + 43, positionBottom + 14);
		}
		doc.setFontStyle('normal');
		if(!protocolDetails.anesthesiaMethod.none && protocolDetails.anesthesiaMethod.anesthesiaMethodDesc){
			lines = doc.splitTextToSize(protocolDetails.anesthesiaMethod.anesthesiaMethodDesc.value, 540);
			doc.text(margins.left + 16, positionBottom + 40, lines);
		}

		doc.line(margins.left, positionBottom + 25, 579, positionBottom + 25);
		positionBottom += 90;
		checkPageLength(doc, 110);

		doc.rect(margins.left, positionBottom, 558, 110);
		doc.setFontSize(10);
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 10, 'J. METHOD OF EUTHANASIA AND/OR DISPOSITION OF ANIMALS AT END OF STUDY');
		doc.setFontStyle('normal');
		doc.setFontSize(9);
		lines = doc.splitTextToSize('Indicate the proposed method, and, if a chemical agent is used, specify the dosage and route of administration. If the method(s) of euthanasia include those not recommended by the AVMA Guideline on Euthanasia, provide scientific justification why such methods must be used, e.g., cervical dislocation of non-anesthetized mice. Indicate the method of carcass disposal if not as MPW.', 550);
		doc.text(margins.left + 16, positionBottom + 20, lines);

		positionBottom += 20;
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom + 30, 'NONE __ (check if none)');

		if(protocolDetails.euthanasiaMethod.none){
			doc.line(margins.left + 34, positionBottom+27, margins.left + 38, positionBottom + 29);
			doc.line(margins.left + 38, positionBottom+29, margins.left + 43, positionBottom + 24);
		}

		doc.setFontStyle('normal');
		if(!protocolDetails.euthanasiaMethod.none && protocolDetails.euthanasiaMethod.euthanasiaMethodDesc){
			lines = doc.splitTextToSize(protocolDetails.euthanasiaMethod.euthanasiaMethodDesc.value, 540);
			doc.text(margins.left + 16, positionBottom + 50, lines);
		}

		doc.line(margins.left, positionBottom + 35, 579, positionBottom + 35);

		positionBottom += 100;

		checkPageLength(doc, 50);
		doc.rect(margins.left, positionBottom, 558, 50);
		positionBottom += 15;
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom, 'K. HAZARDOUS AGENTS:');

		doc.setFontStyle('normal');

		doc.text(margins.left + 120, positionBottom, 'Use of hazardous agents requires the approval of an IC safety specialist. Registration Documents for the use of');
		doc.text(margins.left + 20, positionBottom + 10, 'recombinant DNA or potential human pathogens may be required to be attached at the discretion of the ACUC.');

		doc.text(margins.left + 20, positionBottom + 25, 'NONE __ (check if none)');

		if(!protocolDetails.hazardousAgents.none){
			doc.line(margins.left + 46, positionBottom+22, margins.left + 50, positionBottom + 24);
			doc.line(margins.left + 50, positionBottom+24, margins.left + 55, positionBottom + 20);
		}

		positionBottom += 35;

		checkPageLength(doc, 16.1);
		doc.rect(margins.left, positionBottom, 558, 16.1);
		doc.setFontStyle('bold');
		doc.line(250, positionBottom, 250, positionBottom + 16.1);
		doc.text(margins.left + 235, positionBottom + 10, 'Yes');
		doc.line(280, positionBottom, 280, positionBottom + 16.1);
		doc.text(margins.left + 268, positionBottom + 10, 'No');
		doc.line(310, positionBottom, 310, positionBottom + 16.1);
		doc.text(margins.left + 300, positionBottom + 10, 'List agents and registration document number (if applicable)');

		doc.setFontStyle('normal');

		positionBottom += 16.1;
		checkPageLength(doc, 16.1);
		doc.rect(margins.left, positionBottom, 558, 16.1);
		doc.text(margins.left + 6, positionBottom + 10, 'Radionuclides:');
		doc.line(250, positionBottom, 250, positionBottom + 16.1);
		if(protocolDetails.hazardousAgents.radionuclides){
			doc.text(margins.left + 235, positionBottom + 10, 'Yes');
		}
		doc.line(280, positionBottom, 280, positionBottom + 16.1);
		if(!protocolDetails.hazardousAgents.radionuclides){
			doc.text(margins.left + 268, positionBottom + 10, 'No');
		}
		doc.line(310, positionBottom, 310, positionBottom + 16.1);
		doc.text(margins.left + 300, positionBottom + 10, protocolDetails.hazardousAgents.radionuclidesAgentsAndRegNum || '');

		positionBottom += 16.1;
		checkPageLength(doc, 16.1);
		doc.rect(margins.left, positionBottom, 558, 16.1);
		doc.text(margins.left + 6, positionBottom + 10, 'Biological Agents with Human Pathogenic Potential:');
		doc.line(250, positionBottom, 250, positionBottom + 16.1);
		if(protocolDetails.hazardousAgents.bioAgents){
			doc.text(margins.left + 235, positionBottom + 10, 'Yes');
		}
		doc.line(280, positionBottom, 280, positionBottom + 16.1);
		if(!protocolDetails.hazardousAgents.bioAgents){
			doc.text(margins.left + 268, positionBottom + 10, 'No');
		}
		doc.line(310, positionBottom, 310, positionBottom + 16.1);
		doc.text(margins.left + 300, positionBottom + 10, protocolDetails.hazardousAgents.bioAgentsAgentsAndRegNum || '');

		positionBottom += 16.1;
		checkPageLength(doc, 16.1);
		doc.rect(margins.left, positionBottom, 558, 16.1);
		doc.text(margins.left + 6, positionBottom + 10, 'Hazardous Chemicals or Drugs:');
		doc.line(250, positionBottom, 250, positionBottom + 16.1);
		if(protocolDetails.hazardousAgents.chemicalsAndDrugs){
			doc.text(margins.left + 235, positionBottom + 10, 'Yes');
		}
		doc.line(280, positionBottom, 280, positionBottom + 16.1);
		if(!protocolDetails.hazardousAgents.chemicalsAndDrugs){
			doc.text(margins.left + 268, positionBottom + 10, 'No');
		}
		doc.line(310, positionBottom, 310, positionBottom + 16.1);
		doc.text(margins.left + 300, positionBottom + 10, protocolDetails.hazardousAgents.chemicalsAndDrugsAgentsAndRegNum || '');

		positionBottom += 16.1;
		checkPageLength(doc, 16.1);
		doc.rect(margins.left, positionBottom, 558, 16.1);
		doc.text(margins.left + 6, positionBottom + 10, 'Recombinant DNA:');
		doc.line(250, positionBottom, 250, positionBottom + 16.1);
		if(protocolDetails.hazardousAgents.recombinantDNA){
			doc.text(margins.left + 235, positionBottom + 10, 'Yes');
		}
		doc.line(280, positionBottom, 280, positionBottom + 16.1);
		if(!protocolDetails.hazardousAgents.recombinantDNA){
			doc.text(margins.left + 268, positionBottom + 10, 'No');
		}
		doc.line(310, positionBottom, 310, positionBottom + 16.1);
		doc.text(margins.left + 300, positionBottom + 10, protocolDetails.hazardousAgents.recombinantDNAAgentsAndRegNum || '');

		positionBottom += 16.1;
		checkPageLength(doc, 16.1);
		doc.rect(margins.left, positionBottom, 558, 16.1);
		doc.text(margins.left + 6, positionBottom + 10, 'Study conducted at Animal Biosafety Level (ABSL)');
		doc.line(250, positionBottom, 250, positionBottom + 16.1);
		if(protocolDetails.hazardousAgents.occupationalHealth){
			doc.text(margins.left + 235, positionBottom + 10, protocolDetails.hazardousAgents.occupationalHealth.value || '');
		}

		positionBottom += 16.1;

		checkPageLength(doc, 120);
		doc.rect(margins.left, positionBottom, 558, 120);

		lines = doc.splitTextToSize('Describe the practices and procedures required for the safe handling and disposal of contaminated animals and material associated with this study. For studies involving the administration of radioisotopic materials to animals, provide the DRS number for the Authorized User. Also describe methods for removal of radioactive waste and, if applicable, the monitoring of radioactivity. Use of volatile anesthetics requires a description of scavenging methods used', 550);
		doc.text(margins.left + 6, positionBottom + 10, lines);
		positionBottom += 60;
		doc.text(margins.left + 6, positionBottom, 'Additional safety considerations:');
		if(protocolDetails.hazardousagents.safetyConsiderations){
			lines = doc.splitTextToSize(protocolDetails.hazardousagents.safetyConsiderations.value || '', 550);
			doc.text(margins.left + 6, positionBottom + 10, lines);
		}

		positionBottom += 40;
		doc.text(margins.left + 6, positionBottom, 'Occupational Health and Safety considerations (vaccinations, precautions, etc):');
		if(protocolDetails.hazardousagents.occupationalHealth){
			lines = doc.splitTextToSize(protocolDetails.hazardousagents.occupationalHealth.value || '', 550);
			doc.text(margins.left + 6, positionBottom + 10, lines);
		}

		positionBottom += 40;
		checkPageLength(doc, 30);
		doc.rect(margins.left, positionBottom, 558, 30);
		positionBottom += 15;
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom, 'L. BIOLOGICAL MATERIAL/ANIMAL PRODUCTS FOR USE IN ANIMALS');

		doc.setFontStyle('normal');

		doc.text(margins.left + 320, positionBottom, '(e.g., cell lines, antiserum, etc.): All biologicals to be');
		doc.text(margins.left + 20, positionBottom + 10, 'used in animals must be listed here, regardless of species.');
		doc.setFontStyle('bold');
		doc.text(margins.left + 240, positionBottom + 10, 'NONE __ (check if none)');
		doc.setFontStyle('normal');

		positionBottom += 15;
		checkPageLength(doc, 25);
		doc.rect(margins.left, positionBottom, 558, 25);		
		doc.text(margins.left + 6, positionBottom + 18, 'Material Name:');
		doc.line(180, positionBottom, 180, positionBottom + 25);
		doc.text(186, positionBottom + 18, 'Source:');
		doc.line(280, positionBottom, 280, positionBottom + 25);
		doc.text(margins.left + 280, positionBottom + 10, 'Sterile/Attenuated?');
		doc.text(margins.left + 305, positionBottom + 18, 'Y/N');
		doc.line(400, positionBottom, 400, positionBottom + 25);
		doc.text(margins.left + 385, positionBottom + 18, 'For use in rodents?');

		positionBottom += 25;

		angular.forEach(protocolDetails.bioAnimalMaterial.bioMaterialsUsed, function(material){
			checkPageLength(doc, 16.1);
			doc.rect(margins.left, positionBottom, 558, 16.1);		
			doc.text(margins.left + 6, positionBottom + 10, material.name);
			doc.line(180, positionBottom, 180, positionBottom + 16.1);
			doc.text(186, positionBottom + 10, material.source);
			doc.line(280, positionBottom, 280, positionBottom + 16.1);
			doc.text(margins.left + 305, positionBottom + 10, material.sterileAttenuated ? 'Yes' : 'No');
			doc.line(400, positionBottom, 400, positionBottom + 16.1);
			doc.text(margins.left + 410, positionBottom + 10, material.useInRodent ? 'Yes' : 'No');
			positionBottom += 16.1;
		});

		checkPageLength(doc, 60);
		doc.rect(margins.left, positionBottom, 558, 60);	
		lines = doc.splitTextToSize('I certify that the MAP/RAP/HAP/PCR tested materials to be used in rodents have not been passed through rodent species outside of the animal facility in question and/or the material is derived from the original MAP/RAP/HAP/PCR tested sample. To the best of my knowledge the material remains uncontaminated with rodent pathogens.', 550);
		doc.text(margins.left + 6, positionBottom + 10, lines);
		positionBottom += 40;
		doc.text(margins.left + 390, positionBottom + 10, 'Jagan');
		doc.line(margins.left + 390, positionBottom + 12, margins.left + 430, positionBottom + 12);
		doc.setFontStyle('bold');
		doc.text(margins.left + 430, positionBottom + 10, 'Initials of Principal Investigator');

		positionBottom += 20;
		checkPageLength(doc, 40);
		doc.rect(margins.left, positionBottom, 558, 40);
		positionBottom += 15;
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom, 'M. SPECIAL CONCERNS OR REQUIREMENTS OF THE STUDY:');

		doc.setFontStyle('normal');

		doc.text(margins.left + 280, positionBottom, 'List any special housing, equipment, animal care (i.e., special');
		doc.text(margins.left + 20, positionBottom + 10, 'caging, water, feed, or waste disposal, etc.). Include justification for exemption from participation in the environmental enrichment plan for');
		doc.text(margins.left + 20, positionBottom + 20, 'nonhuman primates or rodents.');
		doc.setFontStyle('bold');
		doc.text(margins.left + 140, positionBottom + 20, 'NONE __ (check if none)');
		if(protocolDetails.specialRequirementOfStudy.none){
			doc.line(margins.left + 168, positionBottom+17, margins.left + 172, positionBottom + 19);
			doc.line(margins.left + 172, positionBottom+19, margins.left + 177, positionBottom + 14);
		}

		positionBottom += 25;
		checkPageLength(doc, 50);
		doc.rect(margins.left, positionBottom, 558, 50);
		positionBottom += 15;
		doc.setFontStyle('bold');
		doc.text(margins.left + 6, positionBottom, 'N. PRINCIPAL INVESTIGATOR CERTIFICATIONS');
		doc.setFontStyle('normal');

		doc.text(margins.left + 225, positionBottom, '(See instructions for further guidance.):');

		positionBottom += 40;
		checkPageLength(doc, 70);
		positionBottom += 15;

		doc.rect(margins.left, positionBottom, 558, 70);

		lines = doc.splitTextToSize('1.  I certify that the individuals working on this project have received the appropriate instructions and training in the biology,handling, and care of this species; aseptic surgical methods and techniques (if necessary); the concept, availability, and the use of research or testing methods that limit the use of animals or minimize distress; the proper use of anesthetics, analgesics, and tranquilizers (if necessary); and procedures for reporting animal welfare concerns, and that these individuals are qualified to perform the required experimental procedures.', 550);
		doc.text(margins.left + 6, positionBottom + 10, lines);

		lines = doc.splitTextToSize('2.  I certify that the room and/or equipment to house this study meets the standards to handle the biohazard level indicated.', 550);
		doc.text(margins.left + 6, positionBottom + 48, lines);

		lines = doc.splitTextToSize('3.  I will inform the IACUC of any proposed significant changes in this study.', 550);
		doc.text(margins.left + 6, positionBottom + 60, lines);

		positionBottom += 70;
		checkPageLength(doc, 40);

		doc.rect(margins.left, positionBottom, 558, 40);

		doc.text(margins.left + 10, positionBottom + 25, 'Principal Investigator:');

		doc.line(margins.left + 100, positionBottom+27, margins.left + 200, positionBottom + 27);

		doc.text(margins.left + 220, positionBottom + 25, 'Signature:');

		doc.line(margins.left + 270, positionBottom+27, margins.left + 370, positionBottom + 27);

		doc.text(margins.left + 400, positionBottom + 25, 'Date:');

		doc.line(margins.left + 430, positionBottom+27, margins.left + 530, positionBottom + 27);

		//Print out PDF
		//doc.output('dataurlnewwindow');
		var pdfName = docName || 'Protocol.pdf';
		doc.save(pdfName);
	};

	function checkPageLength(doc, height) {
		if (positionBottom + height + 20 >= 791) {
			doc.rect(margins.left, currenttPosition, 558, positionBottom - currenttPosition);

			doc.addPage();

			pages++;
			positionBottom = margins.top;
			currenttPosition = margins.top - 20;
			doc.setFontStyle('bold');
			doc.setFontSize(8);
			doc.text(margins.left + 285, 770, '' + pages);
			doc.setFontStyle('normal');
			doc.text(margins.left + 430, 780, footerText);
			doc.setFontSize(9);
		}
	}

	return pdfService;
});