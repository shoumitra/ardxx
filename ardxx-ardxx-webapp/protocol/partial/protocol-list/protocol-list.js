angular.module('protocol').controller('ProtocolListCtrl', function ($scope, $state, toaster, protocolService, DTOptionsBuilder, DTColumnDefBuilder, $cookieStore, $rootScope) {
	$scope.protocols = [];

	var languageOptions = {
		"lengthMenu": '_MENU_ entries per page',
		"search": '<i class="fa fa-search"></i>',
		"paginate": {
			"previous": '<i class="fa fa-angle-left"></i>',
			"next": '<i class="fa fa-angle-right"></i>'
		}
	};
	$scope.isCreating = false;
	$scope.dtOptions = DTOptionsBuilder.newOptions()
		.withDOM('lCfrtip')
		.withOption('language', languageOptions)
		.withColVis()
		.withColVisOption('aiExclude', [0]);

	var roles = {
        chairmanRole: 'CHR_PER',
        piRole: 'PRI_INV',
        memberRole: 'COM_MEM',
		labRole :'LAB_VET',
        researchRole:'RES_SCI'
    };

    $scope.isChairman = $rootScope.currentUser.role.indexOf(roles.chairmanRole) > -1;
    $scope.isCM = $rootScope.currentUser.role.indexOf(roles.memberRole) > -1;
    $scope.isPI = $rootScope.currentUser.role.indexOf(roles.piRole) > -1;
	$scope.isLab = $rootScope.currentUser.role.indexOf(roles.labRole) > -1;
    $scope.isRes = $rootScope.currentUser.role.indexOf(roles.researchRole) > -1;


	protocolService.list()
		.then(function (protocolList) {
		$scope.protocols = protocolList;
		//$cookieStore.put('protocols', $scope.protocols);
	},
		function (data) {
			toaster.pop('error', 'Error', data.errorMessage);
		});

	$scope.createProtocol = function () {
		$scope.isCreating = true;
		protocolService.create($rootScope.currentUser.id)
			.then(function (protocolDetails) {
				$scope.isCreating = false;
			$state.go('protocolDetails', { 'id': protocolDetails.id, 'type': 'new' });
		},
			function (data) {
				$scope.isCreating = false;
				toaster.pop('error', 'Error', data.errorMessage);
			});
	};
});