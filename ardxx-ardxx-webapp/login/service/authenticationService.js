angular.module('login').factory('authenticationService', function (configService, $http, $cookieStore, $rootScope, $timeout, $q) {

    var authenticationService = {};

    authenticationService.SetCredentials = function (user) {
        //var authdata = Base64.encode(username + ':' + password);
        //var currentUser = {
        //    username: user.username,
        //    role:user.
        //};
        //if (username === 'tom') {
        //    currentUser.id = 1;
        //    currentUser.role = 'Principal Investigator';
        //} else if (username === 'cathy') {
        //    currentUser.id = 2;
        //    currentUser.role = 'Chairman';
        //} else if (username === 'mark') {
        //    currentUser.id = 3;
        //    currentUser.role = 'Committee Member';
        //} else {
        //    currentUser.id = 4;
        //    currentUser.role = 'User';
        //}
        $rootScope.globals = {
            'currentUser': user
        };

        //$http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line

        $cookieStore.put('globals', $rootScope.globals);
    };

    authenticationService.clearCredentials = function () {
        $rootScope.globals = {};
        $cookieStore.remove('globals');
        //$http.defaults.headers.common.Authorization = 'Basic ';
    };

    authenticationService.login = function (email, password) {
        var deferred = $q.defer();
        $http.post(configService.getHostName + 'login?email=' + email + '&password=' + password, {})
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function (data) {
		    deferred.reject(data);
		});
        return deferred.promise;
    };

    authenticationService.getUserDetails = function (userId) {
        var deferred = $q.defer();
        $http.get(configService.getHostName + 'users/'+userId)
		.success(function (data) {
		    deferred.resolve(data);
		})
		.error(function () {
		    deferred.reject('Failed to get user details.');
		});
        return deferred.promise;
    };

    return authenticationService;
});