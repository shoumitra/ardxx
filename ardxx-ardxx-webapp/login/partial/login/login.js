angular.module('login').controller('LoginCtrl', function ($scope, $rootScope, $state, authenticationService, $q, $cookieStore) {
    authenticationService.clearCredentials();
    $scope.errorMessage = '';
    $scope.authenticateUser = function () {

        //authenticationService.SetCredentials($scope.user.username, $scope.user.password);
        //$state.go('dashboard',{});
        authenticationService.login($scope.user.username, $scope.user.password)
            .then(function (data) {
            if (data === 'Bad credentials') {
                $scope.errorMessage = data;
            } else {
                getuserDetails(data.id, data.role);
            }
        },
            function (message) {
                $scope.errorMessage = message;
            });
    };

    var getuserDetails = function (userId, role) {
        authenticationService.getUserDetails(userId)
            .then(function (data) {
            var user = {
                id: userId,
                username: data.userName,
                firstName: data.firstName,
                middleName: data.middleName,
                lastName: data.lastName,
                email: data.email,
                role: role
            };
            authenticationService.SetCredentials(user);
            $rootScope.protocols = [];
            $cookieStore.put('protocols', []);
            $state.go('dashboard', {});
        },
            function (message) {
                $scope.errorMessage = message;
            });
    };
});