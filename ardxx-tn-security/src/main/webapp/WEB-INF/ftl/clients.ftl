<#-- @ftlvariable name="clients" type="java.util.List<eu.kielczewski.example.domain.Client>" -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>List of Clients</title>
</head>
<body>
<nav role="navigation">
    <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/client/create">Create a new client</a></li>
    </ul>
</nav>

<h1>List of Clients</h1>

<table>
    <thead>
    <tr>
        <th>E-mail</th>
       
    </tr>
    </thead>
    <tbody>
    <#list clients as client>
    <tr>
        <td><a href="/client/${client.id}">${client.email}</a></td>
       
    </tr>
    </#list>
    </tbody>
</table>
</body>
</html>