package com.tn.security.service.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tn.security.basic.controller.ClientStatus;
import com.tn.security.basic.domain.Role;
import com.tn.security.basic.domain.Client;
import com.tn.security.basic.dto.ClientDTO;
import com.tn.security.basic.dto.UserDTO;
import com.tn.security.basic.repository.RoleRepository;
import com.tn.security.basic.repository.ClientRepository;
import com.tn.security.common.Messages;
import com.tn.security.common.TNUtils;
import com.tn.security.exception.AppException;

@Service
public class ClientServiceImpl implements ClientService
{

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

	@Autowired
	private ClientRepository clientRepository;
	

	@Override
	public Collection<ClientDTO> getAllClients()
	{
		Iterable<Client> clients = clientRepository.findAll();
		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();
		for (Client client : clients)
		{
			ClientDTO clientDTO = new ClientDTO(client);
			clientDTOs.add(clientDTO);
		}

		return clientDTOs;
	}

	
	@Override
	public ClientDTO getClient(Long clientId)
	{
		Client client = clientRepository.findOne(clientId);
		ClientDTO clientDTO = new ClientDTO(client);
		return clientDTO;
	}

	@Override
	public ClientDTO createClient(ClientDTO clientDTO) throws Exception
	{
		ClientDTO clientDTOAfterSave = null;
		try{
			Client client = new Client();
			clientDTOAfterSave = prepareClient(clientDTO, client);
		}catch(Exception e){
			throw new AppException(Messages.properties.getProperty("client.email.exists"));
		}
		return clientDTOAfterSave;
	}

	private ClientDTO prepareClient(ClientDTO clientDTO, Client client)
	{
		client = clientDTO.toDomain(client);
		Client clientAfterSave = clientRepository.save(client);
		ClientDTO clientDTOAfterSave = new ClientDTO(clientAfterSave);
		return clientDTOAfterSave;
	}
	/*

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	RoleRepository roleRepository;

	@Override
	public Optional<Client> getClientById(long id)
	{
		LOGGER.debug("Getting client={}", id);
		return Optional.ofNullable(clientRepository.findOne(id));
	}

	@Override
	public Optional<Client> getClientByEmail(String email)
	{
		LOGGER.debug("Getting client by email={}", email.replaceFirst("@.*", "@***"));
		return clientRepository.findOneByEmailAndStatus(email, ClientStatus.ACTIVE);
	}

	@Override
	public Collection<ClientDTO> getAllClients()
	{
//		Pageable pageable = new PageRequest(page, size,Direction.ASC,"email");
		
		Iterable<Client> clients = clientRepository.findByStatusNot(ClientStatus.DELETED,new Sort("email"));
		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();
		for (Client client : clients)
		{
			ClientDTO clientDTO = new ClientDTO(client);
			clientDTOs.add(clientDTO);
		}

		return clientDTOs;
	}

	@Override
	public List<ClientDTO> getClientsByRole(Long roleId)
	{
		List<Client> clients = clientRepository.findByStatusAndRolesId(ClientStatus.ACTIVE, roleId);
		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();
		for (Client client : clients)
		{
			ClientDTO clientDTO = new ClientDTO(client);
			clientDTOs.add(clientDTO);
		}

		return clientDTOs;
	}

	@Override
	public ClientDTO getClient(Long clientId)
	{
		Client client = clientRepository.findOne(clientId);
		ClientDTO clientDTO = new ClientDTO(client);
		return clientDTO;
	}

	@Override
	public ClientDTO createClient(ClientDTO clientDTO) throws Exception
	{
		Client client = clientRepository.findOneByEmail(clientDTO.getEmail());
		if (client == null)
		{
			client = new Client();
			ClientDTO clientDTOAfterSave = prepareClient(clientDTO, client);
			return clientDTOAfterSave;
		} else
		{
			throw new AppException(Messages.properties.getProperty("client.email.exists"));
		}

	}

	@Override
	public ClientDTO updateClient(ClientDTO clientDTO)
	{
		Client client = clientRepository.findOne(clientDTO.getId());
		
		
		ClientDTO clientDTOAfterSave = prepareClient(clientDTO, client);
		return clientDTOAfterSave;
	}

	private ClientDTO prepareClient(ClientDTO clientDTO, Client client)
	{
		client = clientDTO.toDomain(client);

				
		Client clientAfterSave = clientRepository.save(client);
		ClientDTO clientDTOAfterSave = new ClientDTO(clientAfterSave);
		return clientDTOAfterSave;
	}

	@Override
	public List<ClientDTO> getPIClients(String roleName)
	{
		List<Client> clients = clientRepository.findByStatusAndRolesRoleName(ClientStatus.ACTIVE, roleName);
		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();
		for (Client client : clients)
		{
			ClientDTO clientDTO = new ClientDTO(client);
			clientDTOs.add(clientDTO);
		}

		return clientDTOs;
	}

	@Override
	public List<ClientDTO> getClientsByRoles(List<Long> roleIds)
	{
		List<Client> clients = clientRepository.findByStatusAndRolesIdIn(roleIds);
		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();
		for (Client client : clients)
		{
			ClientDTO clientDTO = new ClientDTO(client);
			clientDTOs.add(clientDTO);
		}

		return clientDTOs;
	}

*/


	@Override
	public ClientDTO updateClient(ClientDTO clientDTO) {
		// TODO Auto-generated method stub
		Client client = clientRepository.findOne(clientDTO.getId());
		ClientDTO clientDTOAfterSave = prepareClient(clientDTO, client);
		return clientDTOAfterSave;
	}
	


	
	
	/*private static final Logger LOGGER = LoggerFactory.getLogger(ClientServiceImpl.class);

	@Autowired
	private ClientRepository clientRepository;

	
	@Override
	public Collection<ClientDTO> getAllClients()
	{
		Iterable<Client> clients = clientRepository.findByStatusNot(new Sort("email"));
		List<ClientDTO> clientDTOs = new ArrayList<ClientDTO>();
		for (Client client : clients)
		{
			ClientDTO clientDTO = new ClientDTO(client);
			clientDTOs.add(clientDTO);
		}

		return clientDTOs;
	}

	
	@Override
	public ClientDTO getClient(Long clientId)
	{
		Client client = clientRepository.findOne(clientId);
		ClientDTO clientDTO = new ClientDTO(client);
		return clientDTO;
	}

	@Override
	public ClientDTO createClient(ClientDTO clientDTO) throws Exception
	{
		Client client = clientRepository.findOneByEmail(clientDTO.getEmail());
		if (client == null)
		{
			client = new Client();
			ClientDTO clientDTOAfterSave = prepareClient(clientDTO, client);
			return clientDTOAfterSave;
		} else
		{
			throw new AppException(Messages.properties.getProperty("client.email.exists"));
		}

	}

	private ClientDTO prepareClient(ClientDTO clientDTO, Client client)
	{
		client = clientDTO.toDomain(client);

		Client clientAfterSave = clientRepository.save(client);
		ClientDTO clientDTOAfterSave = new ClientDTO(clientAfterSave);
		return clientDTOAfterSave;
	}
*/

	
	
	
	

}
