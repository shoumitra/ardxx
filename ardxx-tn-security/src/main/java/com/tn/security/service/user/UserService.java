package com.tn.security.service.user;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.tn.security.basic.domain.User;
import com.tn.security.basic.dto.UserDTO;

public interface UserService
{

	/**
	 * Method to get user for a given user id
	 * 
	 * @param id
	 * @return
	 */
	Optional<User> getUserById(long id);

	/**
	 * Method to get user for a given email
	 * 
	 * @param email
	 * @return
	 */
	Optional<User> getUserByEmail(String email);

	/**
	 * Method to get all the User.
	 * 
	 * @return Returns {@link UserDTO} all the user information.s
	 */
	Collection<UserDTO> getAllUsers();

	/**
	 * Method to get all the PI user
	 * 
	 * @param roleId
	 * @return
	 */
	public List<UserDTO> getUsersByRole(Long roleId);

	/**
	 * Method to get User for given user id
	 * 
	 * @param userId
	 *            User Id
	 * @return Returns {@link UserDTO} user information
	 */
	UserDTO getUser(Long userId);

	/**
	 * Method to create user.
	 * 
	 * @param user
	 *            {@link UserDTO}
	 * @return Returns the {@link UserDTO}
	 */
	UserDTO createUser(UserDTO userDTO) throws Exception;

	UserDTO updateUser(UserDTO userDTO);

	List<UserDTO> getPIUsers(String roleName);

	List<UserDTO> getUsersByRoles(List<Long> roleIds);
}
