package com.tn.security.service.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tn.security.basic.domain.Role;
import com.tn.security.basic.repository.RoleRepository;

/**
 * Service Implementation for Role Management.
 * 
 * @author Raj
 *
 */
@Service
public class RoleServiceImpl implements RoleService
{

	@Autowired
	RoleRepository roleRepository;

	@Override
	public Iterable<Role> getAllRoles()
	{

		return roleRepository.findAll();
	}

	@Override
	public Role getRoleById(Long roleId)
	{
		return roleRepository.findOne(roleId);
	}

}
