package com.tn.security.service.role;

import java.util.List;

import com.tn.security.basic.domain.Role;

public interface RoleService
{

	/**
	 * Method to get all the Roles
	 * 
	 * @return Returns List of {@link Role}s
	 */
	public Iterable<Role> getAllRoles();

	/**
	 * Method to get a Role based on the role id
	 * 
	 * @param Id
	 *            Role id
	 * @return Returns {@link Role}
	 */
	public Role getRoleById(Long Id);
}
