package com.tn.security.service.client;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.tn.security.basic.domain.Client;
import com.tn.security.basic.dto.ClientDTO;
 
public interface ClientService
{/* 

	*//**
	 * Method to get client for a given client id
	 * 
	 * @param id
	 * @return
	 *//*
	Optional<Client> getClientById(long id);

	*//**
	 * Method to get client for a given email
	 * 
	 * @param email
	 * @return
	 *//*
	Optional<Client> getClientByEmail(String email);

	*//**
	 * Method to get all the Client.
	 * 
	 * @return Returns {@link ClientDTO} all the client information.s
	 *//*
	Collection<ClientDTO> getAllClients();

	*//**
	 * Method to get all the PI client
	 * 
	 * @param roleId
	 * @return
	 *//*
	public List<ClientDTO> getClientsByRole(Long roleId);

	*//**
	 * Method to get Client for given client id
	 * 
	 * @param clientId
	 *            Client Id
	 * @return Returns {@link ClientDTO} client information
	 *//*
	ClientDTO getClient(Long clientId);

	*//**
	 * Method to create client.
	 * 
	 * @param client
	 *            {@link ClientDTO}
	 * @return Returns the {@link ClientDTO}
	 *//*
	ClientDTO createClient(ClientDTO clientDTO) throws Exception;

	ClientDTO updateClient(ClientDTO clientDTO);

	List<ClientDTO> getPIClients(String roleName);

	List<ClientDTO> getClientsByRoles(List<Long> roleIds);
*/
	


	/**
	 * Method to get client for a given client id
	 * 
	 * @param id
	 * @return
	 */
	//Optional<Client> getClientById(long id);

	/**
	 * Method to get client for a given email
	 * 
	 * @param email
	 * @return
	 */
	//Optional<Client> getClientByEmail(String email);

	/**
	 * Method to get all the Client.
	 * 
	 * @return Returns {@link ClientDTO} all the client information.s
	 */
	Collection<ClientDTO> getAllClients();

		/**
	 * Method to get Client for given client id
	 * 
	 * @param clientId
	 *            Client Id
	 * @return Returns {@link ClientDTO} client information
	 */
	ClientDTO getClient(Long clientId);

	/**
	 * Method to create client.
	 * 
	 * @param client
	 *            {@link ClientDTO}
	 * @return Returns the {@link ClientDTO}
	 */
	ClientDTO createClient(ClientDTO clientDTO) throws Exception;

	ClientDTO updateClient(ClientDTO clientDTO);

	

}
