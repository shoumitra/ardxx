package com.tn.security.exception;

/**
 * 
 * @author Raj
 *
 */
public class AppException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String errorCode;
	
	public AppException(String errorCode)
	{
		this.errorCode = errorCode;
	}
	
	@Override
	public String getMessage()
	{
		return this.errorCode;
	}
	
	@Override
	public String getLocalizedMessage()
	{
		return this.errorCode;
	}

}
