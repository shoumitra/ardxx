package com.tn.security.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class Messages
{

	public static Properties properties = new Properties();

	static
	{// load a properties file
		try (InputStream input = Messages.class.getClassLoader().getResourceAsStream("messages.properties"))
		{
			properties.load(input);

		} catch (IOException e)
		{
			System.out.println("error loading file");
		}
	}

}
