package com.tn.security.basic.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tn.security.basic.dto.ClientDTO;
import com.tn.security.exception.AppException;
import com.tn.security.exception.ErrorPayload;
import com.tn.security.service.client.ClientService;

/**
 * Rest Controller for Client Management.
 * 
 * @author Arun
 *
 */
@RestController
@RequestMapping(value = "/clients")
public class ClientController
{/*

	@Autowired
	private ClientService clientService;
	@Autowired
	private EmailUtil emailUtil;

	*//**
	 * API to get clients by given role id
	 * 
	 * @param roleId
	 * @return
	 *//*
	@RequestMapping(value = "/piclients/{piroleid}", method = RequestMethod.GET)
	public List<ClientDTO> piclients(@PathVariable("piroleid") Long roleId)
	{
		return clientService.getClientsByRole(roleId);
	}

	@RequestMapping(value = "/clientsforrole/{roleid}", method = RequestMethod.GET)
	public List<ClientDTO> getClientsForGivenRole(@PathVariable("roleid") Long roleId)
	{
		return clientService.getClientsByRole(roleId);
	}

	@RequestMapping(value = "/clientsforrole/rolename/{rolename}", method = RequestMethod.GET)
	public List<ClientDTO> getClientsForGivenRoleName(@PathVariable("rolename") String roleName)
	{
		return clientService.getPIClients(roleName);
	}

	@RequestMapping(value = "/clientsforrole/roleids/{roleid}", method = RequestMethod.GET)
	public List<ClientDTO> getClientsForGivenRoles(@PathVariable("roleid") List<Long> roleId)
	{
		return clientService.getClientsByRoles(roleId);
	}

	*//**
	 * Method to get all the clients.
	 * 
	 * @return
	 *//*
	@RequestMapping(method = RequestMethod.GET)
	public Collection<ClientDTO> getAllClients()
	{
		return clientService.getAllClients();
	}

	*//**
	 * Get Client Information for a given client idd
	 * 
	 * @param clientId
	 *            Client Id
	 * @return Returns {@link ClientDTO} client information
	 *//*
	@RequestMapping(value = "/{clientid}", method = RequestMethod.GET)
	public ClientDTO getClientById(@PathVariable("clientid") Long clientId)
	{
		return clientService.getClient(clientId);
	}

	@PreAuthorize("hasAnyAuthority('SUP_ADM')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> createClient(@RequestBody ClientDTO clientDTO)
	{
		try
		{
			ClientValidator clientValidator = new ClientValidator();
			List<String> errorCodes = clientValidator.validateCreateClient(clientDTO);
			if (errorCodes.isEmpty())
			{
				ClientDTO newClientDTO = clientService.createClient(clientDTO);

			

					emailUtil.createEmailTemplate(new String[] { newClientDTO.getEmail() }, newClientDTO.getName(), newClientDTO.getPhoneNumber(),
							newClientDTO.getEmail(), clientDTO.getBusinessAddress());

				return new ResponseEntity<Object>(newClientDTO, HttpStatus.OK);
			} else
			{
				return new ResponseEntity<Object>(new ErrorPayload(errorCodes.toArray(new String[errorCodes.size()])),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception exception)
		{
			exception.printStackTrace();
			if (exception instanceof AppException)
			{
				return new ResponseEntity<Object>(new ErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
	}

	@PreAuthorize("@currentClientServiceImpl.canChangeStatus(principal, #clientDTO)")
	@RequestMapping(method = RequestMethod.PUT)
	public ClientDTO updateClient(@RequestBody ClientDTO clientDTO)
	{
		return clientService.updateClient(clientDTO);
	}

*/
	
	

		@Autowired
		private ClientService clientService;

		/**
		 * Method to get all the clients.
		 * 
		 * @return
		 */
		@RequestMapping(method = RequestMethod.GET)
		public Collection<ClientDTO> getAllClients()
		{
			return clientService.getAllClients();
		}

		/**
		 * Get Client Information for a given client idd
		 * 
		 * @param clientId
		 *            Client Id
		 * @return Returns {@link ClientDTO} client information
		 */
		@RequestMapping(value = "/{clientid}", method = RequestMethod.GET)
		public ClientDTO getClientById(@PathVariable("clientid") Long clientId)
		{
			return clientService.getClient(clientId);
		}

		

		
		@RequestMapping(method = RequestMethod.POST)
		 public ResponseEntity<Object> createClient(@RequestBody ClientDTO clientDTO)
		 {
		  try
		  {
			ClientDTO newClientDTO = clientService.createClient(clientDTO);

		    return new ResponseEntity<Object>(newClientDTO, HttpStatus.OK);

		  } catch (Exception exception)
		  {
		   exception.printStackTrace();
		   if (exception instanceof AppException)
		   {
		    return new ResponseEntity<Object>(new ErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		   } else
		   {
		    return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		   }

		  }
		 }
		
		
		
		/*@PreAuthorize("@currentClientServiceImpl.canChangeStatus(principal, #clientDTO)")
		@RequestMapping(method = RequestMethod.PUT)
		public ClientDTO updateClient(@RequestBody ClientDTO clientDTO)
		{
			return clientService.updateClient(clientDTO);
		}*/


}
