package com.tn.security.basic.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tn.commons.email.util.EmailUtil;
import com.tn.security.basic.domain.validator.UserValidator;
import com.tn.security.basic.dto.UserDTO;
import com.tn.security.email.dto.Mail;
import com.tn.security.exception.AppException;
import com.tn.security.exception.ErrorPayload;
import com.tn.security.service.user.UserService;

/**
 * Rest Controller for User Management.
 * 
 * @author Raj
 *
 */
@RestController
@RequestMapping(value = "/users")
public class UserController
{

	@Autowired
	private UserService userService;
	@Autowired
	private EmailUtil emailUtil;

	/**
	 * API to get users by given role id
	 * 
	 * @param roleId
	 * @return
	 */
	@RequestMapping(value = "/piusers/{piroleid}", method = RequestMethod.GET)
	public List<UserDTO> piusers(@PathVariable("piroleid") Long roleId)
	{
		return userService.getUsersByRole(roleId);
	}

	@RequestMapping(value = "/usersforrole/{roleid}", method = RequestMethod.GET)
	public List<UserDTO> getUsersForGivenRole(@PathVariable("roleid") Long roleId)
	{
		return userService.getUsersByRole(roleId);
	}

	@RequestMapping(value = "/usersforrole/rolename/{rolename}", method = RequestMethod.GET)
	public List<UserDTO> getUsersForGivenRoleName(@PathVariable("rolename") String roleName)
	{
		return userService.getPIUsers(roleName);
	}

	@RequestMapping(value = "/usersforrole/roleids/{roleid}", method = RequestMethod.GET)
	public List<UserDTO> getUsersForGivenRoles(@PathVariable("roleid") List<Long> roleId)
	{
		return userService.getUsersByRoles(roleId);
	}

	/**
	 * Method to get all the users.
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public Collection<UserDTO> getAllUsers()
	{
		return userService.getAllUsers();
	}

	/**
	 * Get User Information for a given user idd
	 * 
	 * @param userId
	 *            User Id
	 * @return Returns {@link UserDTO} user information
	 */
	@RequestMapping(value = "/{userid}", method = RequestMethod.GET)
	public UserDTO getUserById(@PathVariable("userid") Long userId)
	{
		return userService.getUser(userId);
	}

	@PreAuthorize("hasAnyAuthority('SUP_ADM')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Object> createUser(@RequestBody UserDTO userDTO)
	{
		try
		{
			UserValidator userValidator = new UserValidator();
			List<String> errorCodes = userValidator.validateCreateUser(userDTO);
			if (errorCodes.isEmpty())
			{
				UserDTO newUserDTO = userService.createUser(userDTO);

			

					emailUtil.createEmailTemplate(new String[] { newUserDTO.getEmail() }, newUserDTO.getFirstName(), newUserDTO.getLastName(),
							newUserDTO.getEmail(), userDTO.getPasswordHash());

				return new ResponseEntity<Object>(newUserDTO, HttpStatus.OK);
			} else
			{
				return new ResponseEntity<Object>(new ErrorPayload(errorCodes.toArray(new String[errorCodes.size()])),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch (Exception exception)
		{
			exception.printStackTrace();
			if (exception instanceof AppException)
			{
				return new ResponseEntity<Object>(new ErrorPayload(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
			} else
			{
				return new ResponseEntity<Object>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
	}

	@PreAuthorize("@currentUserServiceImpl.canChangeStatus(principal, #userDTO)")
	@RequestMapping(method = RequestMethod.PUT)
	public UserDTO updateUser(@RequestBody UserDTO userDTO)
	{
		return userService.updateUser(userDTO);
	}

}
