package com.tn.security.basic.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tn.security.basic.controller.ClientStatus;
import com.tn.security.basic.domain.Role;
import com.tn.security.basic.domain.Client;

public class ClientDTO implements Serializable
{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String clientName;
	private String chargeCode;
	private String email;
	private String address;
	private String city;
	private String state;
	private String zip;
	private String phone1;
	private String phone2;
	private String billingAddress;
	private String billingContact;
	
	public ClientDTO()
	{
	}
	/**
	 * 
	 * @param client
	 */
	public ClientDTO(Client client)
	{
		this.id = client.getId();
		this.clientName = client.getClientName();
		this.chargeCode = client.getChargeCode();
		this.email = client.getEmail();
		this.address = client.getAddress();
		this.city = client.getCity();
		this.state = client.getState();
		this.zip = client.getZip();
		this.phone1 = client.getPhone1();
		this.phone2 = client.getPhone2();
		this.billingAddress = client.getBillingAddress();
		this.billingContact = client.getBillingContact();
	}

	/**
	 * 
	 * @param client
	 * @return
	 */
	public Client toDomain(Client client)
	{
		client.setId(this.id);
		client.setClientName(this.clientName);
		client.setChargeCode(this.chargeCode);
		client.setEmail(this.email);
		client.setAddress(this.address);
		client.setCity(this.city);
		client.setState(this.state);
		client.setZip(this.zip);
		client.setPhone1(this.phone1);
		client.setPhone2(this.phone2);
		client.setBillingAddress(this.billingAddress);
		client.setBillingContact(this.billingContact);
		return client;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the chargeCode
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode the chargeCode to set
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the clientName
	 */
	public String getClientName() {
		return clientName;
	}

	/**
	 * @param clientName the clientName to set
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the phone1
	 */
	public String getPhone1() {
		return phone1;
	}

	/**
	 * @param phone1 the phone1 to set
	 */
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	/**
	 * @return the phone2
	 */
	public String getPhone2() {
		return phone2;
	}

	/**
	 * @param phone2 the phone2 to set
	 */
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress() {
		return billingAddress;
	}

	/**
	 * @param billingAddress the billingAddress to set
	 */
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	/**
	 * @return the billingContact
	 */
	public String getBillingContact() {
		return billingContact;
	}

	/**
	 * @param billingContact the billingContact to set
	 */
	public void setBillingContact(String billingContact) {
		this.billingContact = billingContact;
	}

		
	

}
