package com.tn.security.basic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tn.security.basic.domain.Role;
import com.tn.security.service.role.RoleService;

/**
 * Rest Controller to manage Roles
 * 
 * @author Raj
 *
 */
@RestController
@RequestMapping("/roles")
public class RoleController
{

	@Autowired
	RoleService roleService;

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<Role> getAllRoles()
	{
		return roleService.getAllRoles();
	}

	@RequestMapping(value = "/{roleid}", method = RequestMethod.GET)
	public Role getRolesById(@PathVariable("roleid") Long roleId)
	{
		return roleService.getRoleById(roleId);
	}

}
