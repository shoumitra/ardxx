package com.tn.security.basic.domain.validator;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.tn.security.basic.dto.ClientDTO;
import com.tn.security.common.Messages;
import com.tn.security.common.TNUtils;

/**
 * Validator class to validate Client values
 * @author Arun
 *
 */
public class ClientValidator
{
	/**
	 * Method to validate create client input values
	 * @param clientDTO
	 * @return Returns list of error codes
	 */
	public List<String> validateCreateClient(ClientDTO clientDTO)
	{
		List<String> errorCode = new ArrayList<String>();

		if (TNUtils.isEmpty(clientDTO.getEmail()))
		{
			errorCode.add(Messages.properties.getProperty("email.is.empty"));
		}
		else if(!isValidEmail(clientDTO.getEmail()))
		{
			errorCode.add(Messages.properties.getProperty("email.invalid"));
		}
		if(!TNUtils.isEmpty(clientDTO.getPhone1()) && !isNumeric(clientDTO.getPhone1()))
		{
			errorCode.add(Messages.properties.getProperty("phonenumber.invalid"));
		}
		if(!TNUtils.isEmpty(clientDTO.getPhone2()) && !isNumeric(clientDTO.getPhone2()))
		{
			errorCode.add(Messages.properties.getProperty("phonenumber.invalid"));
		}
		if(!TNUtils.isEmpty(clientDTO.getBillingContact()) && !isNumeric(clientDTO.getBillingContact()))
		{
			errorCode.add(Messages.properties.getProperty("phonenumber.invalid"));
		}
		
		return errorCode;
	}
	
	public static boolean isNumeric(String value)
	{
		return value.matches("-?\\d+(\\.\\d+)?");
	}
	
	public static boolean isValidEmail(String email)
	{
		try
		{
			InternetAddress internetAddress = new InternetAddress(email);
			internetAddress.validate();
		} catch (AddressException e)
		{
			return false;
		}
		return true;
	}
	
}
