package com.tn.security.basic.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.tn.security.basic.controller.UserStatus;
import com.tn.security.basic.domain.Role;
import com.tn.security.basic.domain.User;

public class UserDTO implements Serializable
{

	private static final long serialVersionUID = 1L;

	public UserDTO()
	{

	}

	public UserDTO(User user)
	{
		this.id = user.getId();
		this.email = user.getEmail();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.middleName = user.getMiddleName();
		this.roles = user.getRoles();
		this.status = user.getStatus();
		this.externalUser = user.isExternalUser();
		this.phoneNumber = user.getPhoneNumber();
	}

	public User toDomain(User user)
	{
		user.setId(this.id);
		user.setEmail(this.email);
		user.setFirstName(this.firstName);
		user.setLastName(this.lastName);
		user.setMiddleName(this.middleName);
		user.setRoles(this.roles);
		// user.setPasswordHash(this.passwordHash);
		user.setStatus(this.status);
		user.setExternalUser(this.externalUser);
		user.setPhoneNumber(this.phoneNumber);
		
		return user;
	}

	private Long id;
	private String email;
	private String firstName;
	private String middleName;
	private String lastName;
	private String passwordHash;
	private UserStatus status;
	private boolean externalUser;
	private String phoneNumber;
	private List<Role> roles = new ArrayList<Role>();
	private List<Long> roleIds = new ArrayList<Long>();

	public boolean isExternalUser()
	{
		return externalUser;
	}

	public void setExternalUser(boolean externalUser)
	{
		this.externalUser = externalUser;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	/*
	 * public String getUsername() { return username; } public void setUsername(String username) { this.username = username; }
	 */
	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public List<Role> getRoles()
	{
		return roles;
	}

	public void setRoles(List<Role> roles)
	{
		this.roles = roles;
	}

	public String getPasswordHash()
	{
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash)
	{
		this.passwordHash = passwordHash;
	}

	public UserStatus getStatus()
	{
		return status;
	}

	public void setStatus(UserStatus status)
	{
		this.status = status;
	}

	public List<Long> getRoleIds()
	{
		return roleIds;
	}

	public void setRoleIds(List<Long> roleIds)
	{
		this.roleIds = roleIds;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

}
