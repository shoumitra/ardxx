package com.tn.security.basic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tn.security.basic.domain.Role;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Long>
{

	public Role findByRoleName(@Param("roleName") String roleName);
}
