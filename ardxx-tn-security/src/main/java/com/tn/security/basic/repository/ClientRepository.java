package com.tn.security.basic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tn.security.basic.controller.ClientStatus;
import com.tn.security.basic.domain.Client;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, Long>
{

	//Optional<Client> findOneByEmailAndStatus(String email,ClientStatus status);

	//public List<Client> findByStatusAndRolesId(@Param("status") ClientStatus status, @Param("id") Long id);

	//@Query(value="select * from client where status = 'ACTIVE' and id in (select client_id from client_role where role_id in(:id))",nativeQuery=true)
	//public List<Client> findByStatusAndRolesIdIn(@Param("id") List<Long> id);
	
	//public List<Client> findByStatusAndRolesRoleName(@Param("status") ClientStatus status, @Param("roleName") String roleName);
	
	public Client findOneByEmail(@Param("email") String email);
	
	//public List<Client> findByStatusNot(Sort sort);
	
	//public Client findOne(Long id);



	
	/*public Client findOneByEmail(@Param("email") String email);
	
	public List<Client> findByStatusNot(Sort sort);
	
	public Client findOne(Long id);
*/
}
