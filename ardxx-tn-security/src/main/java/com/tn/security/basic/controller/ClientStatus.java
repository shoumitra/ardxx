package com.tn.security.basic.controller;

/**
 * 
 * @author Arun
 *
 */
public enum ClientStatus
{
	ACTIVE, INACTIVE, DELETED
}
