Example Spring Boot Security
============================

The application showing how to use Spring Boot with Spring Security for common needs, such as:

* Customized login form
* DAO-based authentication
* Basic "remember me" authentication
* URL-based security
* Method-level security


Quick start
-----------
1. `mvn spring-boot:run`
3. Point your browser to [http://localhost:8080/](http://localhost:8080/)
